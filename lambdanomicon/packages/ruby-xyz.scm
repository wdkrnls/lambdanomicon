;;; Lambdanomicon
;;; Copyright © 2020 Kyle Andrews <kyle.c.andrews@gmail.com>
;;;
;;; This file is part of Lambdanomicon
;;;
;;; Lambdanomicon is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.


(define-module (lambdanomicon packages ruby-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system ruby)
  #:use-module ((guix licenses) #:prefix license:))

(define-public ruby-daff
  (package
    (name "ruby-daff")
    (version "1.3.27")
    (source
     (origin
       (method url-fetch)
       (uri (rubygems-uri "daff" version))
       (sha256
        (base32
	 "1bj14sp4jf6nsmzvp9nq08c639bmf8gjnzc1iphqvmig9zra60k4"))))
    (build-system ruby-build-system)
    (synopsis "Diff and patch tables")
    (description "Diff and patch tables")
    (home-page "https://github.com/paulfitz/daff")
    (license license:expat)))
