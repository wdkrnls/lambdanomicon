;;; Lambdanomicon
;;; Copyright © 2019 Kyle Andrews <kyle.c.andrews@gmail.com>
;;;
;;; This file is part of Lambdanomicon
;;;
;;; Lambdanomicon is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.

(define-module (lambdanomicon packages cran)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system r)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages graph)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages bioconductor)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages ocr)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages video)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages image)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages gcc))

(define-public r-piton
  (package
   (name "r-piton")
   (version "1.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "piton" version))
     (sha256
      (base32 "1krf6zi238m275nxjlpyayv8y2drbgs2kg19dpkqm0lmlz5y5ar8"))))
   (properties `((upstream-name . "piton")))
   (build-system r-build-system)
   (propagated-inputs (list r-rcpp))
   (home-page "https://github.com/Ironholds/piton")
   (synopsis "Parsing Expression Grammars in Rcpp")
   (description
    "This package provides a wrapper around the 'Parsing Expression Grammar Template
Library', a C++11 library for generating Parsing Expression Grammars, that makes
it accessible within Rcpp.  With this, developers can implement their own
grammars and easily expose them in R packages.")
   (license license:expat)))


(define-public r-tidyxl
  (package
    (name "r-tidyxl")
    (version "1.0.7")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "tidyxl" version))
       (sha256
	(base32 "1wg8h5fd2hd8ilshrbcs99q18p15687dx8j51v5pyd30pg5cab7b"))))
    (properties `((upstream-name . "tidyxl")))
    (build-system r-build-system)
    (propagated-inputs (list r-piton r-rcpp))
    (native-inputs (list r-knitr))
    (home-page "https://github.com/nacnudus/tidyxl")
    (synopsis "Read Untidy Excel Files")
    (description
     "Imports non-tabular from Excel files into R.  Exposes cell content, position and
formatting in a tidy structure for further manipulation.  Tokenizes Excel
formulas.  Supports '.xlsx' and '.xlsm' via the embedded 'RapidXML' C++ library
<http://rapidxml.sourceforge.net>.  Does not support '.xlsb' or '.xls'.")
    (license license:gpl3)))

(define-public r-unpivotr
  (package
   (name "r-unpivotr")
   (version "0.6.2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "unpivotr" version))
     (sha256
      (base32 "1paqx2jn8gh4nd4zm8iwxpf38znzwwqli62ww7f3k5vwkf34r078"))))
   (properties `((upstream-name . "unpivotr")))
   (build-system r-build-system)
   (propagated-inputs
    (list r-cellranger
          r-dplyr
          r-forcats
          r-magrittr
          r-pillar
          r-purrr
          r-rlang
          r-tibble
          r-tidyr
          r-tidyselect
          r-xml2))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/nacnudus/unpivotr")
   (synopsis "Unpivot Complex and Irregular Data Layouts")
   (description
    "Tools for converting data from complex or irregular layouts to a columnar
structure.  For example, tables with multilevel column or row headers, or
spreadsheets.  Header and data cells are selected by their contents and
position, as well as formatting and comments where available, and are associated
with one other by their proximity in given directions.  Functions for data
frames and HTML tables are provided.")
   (license license:expat)))

(define-public r-unglue
  (package
   (name "r-unglue")
   (version "0.1.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "unglue" version))
     (sha256
      (base32 "0w8ld4xllx0lj1jz8i2sj92f8136hlwri1d8ldpg1ymxj7aw93vg"))))
   (properties `((upstream-name . "unglue")))
   (build-system r-build-system)
   (home-page "https://cran.r-project.org/package=unglue")
   (synopsis "Extract Matched Substrings Using a Pattern")
   (description
    "Use syntax inspired by the package 'glue' to extract matched
substrings in a more compact way than by using regular expressions.")
   (license license:gpl3)))

(define-public r-vcdextra
  (package
   (name "r-vcdextra")
   (version "0.8-0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "vcdExtra" version))
     (sha256
      (base32 "1wv1ypjdvw639jlphwfpqgpjkk4ki98l2lfbpncsbpy9jpknjsmk"))))
   (properties `((upstream-name . "vcdExtra")))
   (build-system r-build-system)
   (propagated-inputs (list r-ca r-gnm r-mass r-vcd))
   (native-inputs (list r-knitr))
   (home-page "https://friendly.github.io/vcdExtra/")
   (synopsis "'vcd' Extensions and Additions")
   (description
    "This package provides additional data sets, methods and documentation to
complement the 'vcd' package for Visualizing Categorical Data and the 'gnm'
package for Generalized Nonlinear Models.  In particular, 'vcdExtra' extends
mosaic, assoc and sieve plots from 'vcd' to handle 'glm()' and 'gnm()' models
and adds a 3D version in 'mosaic3d'.  Additionally, methods are provided for
comparing and visualizing lists of 'glm' and 'loglm' objects.  This package is
now a support package for the book, \"Discrete Data Analysis with R\" by Michael
Friendly and David Meyer.")
   (license license:gpl2+)))

(define-public r-ca
  (package
   (name "r-ca")
   (version "0.71.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ca" version))
     (sha256
      (base32
       "095lk7p2b9835hc3a03c4019wg0baf0dhz6c2vqpaq1m9k4jy304"))))
   (properties `((upstream-name . "ca")))
   (build-system r-build-system)
   (home-page "http://www.carme-n.org/")
   (synopsis
    "Simple, Multiple and Joint Correspondence Analysis")
   (description
    "Computation and visualization of simple, multiple and joint correspondence analysis.")
   (license (list license:gpl2+ license:gpl3+))))

(define-public r-gnm
  (package
   (name "r-gnm")
   (version "1.1-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gnm" version))
     (sha256
      (base32
       "1lddsdsg43wpg681s906i4xqqfbjdd1qb9ml5dfprb02i1806aa2"))))
   (properties `((upstream-name . "gnm")))
   (build-system r-build-system)
   (propagated-inputs
    (list r-mass r-matrix r-nnet r-qvcalc r-relimp))
   (home-page "https://github.com/hturner/gnm")
   (synopsis "Generalized Nonlinear Models")
   (description
    "This package provides functions to specify and fit generalized nonlinear models, including models with multiplicative interaction terms such as the UNIDIFF model from sociology and the AMMI model from crop science, and many others.  Over-parameterized representations of models are used throughout; functions are provided for inference on estimable parameter combinations, as well as standard methods for diagnostics etc.")
   (license (list license:gpl2+ license:gpl3+))))

(define-public r-qvcalc
  (package
   (name "r-qvcalc")
   (version "1.0.2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "qvcalc" version))
     (sha256
      (base32
       "0banzv78kb53ybfbh1hmgx7kxvpbbdxzg4dsn7vrwhmxs72srkch"))))
   (properties `((upstream-name . "qvcalc")))
   (build-system r-build-system)
   (home-page "https://davidfirth.github.io/qvcalc")
   (synopsis
    "Quasi Variances for Factor Effects in Statistical Models")
   (description
    "This package provides functions to compute quasi variances and associated measures of approximation error.")
   (license (list license:gpl2+ license:gpl3+))))

(define-public r-relimp
  (package
   (name "r-relimp")
   (version "1.0-5")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "relimp" version))
     (sha256
      (base32
       "0ydn82g8xdqzhf34187080bbpcpw1zdjbj2i3dv1d6d35vvprb5c"))))
   (properties `((upstream-name . "relimp")))
   (build-system r-build-system)
   (home-page "http://warwick.ac.uk/relimp")
   (synopsis
    "Relative Contribution of Effects in a Regression Model")
   (description
    "This package provides functions to facilitate inference on the relative importance of predictors in a linear or generalized linear model, and a couple of useful Tcl/Tk widgets.")
   (license license:gpl2+)))

(define-public r-rpicosat
  (package
   (name "r-rpicosat")
   (version "1.0.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "rpicosat" version))
     (sha256
      (base32
       "1zj2d6jairmvya91vhv9kpkf34zmzl9vlha5yvfjj0j0apmqc0li"))))
   (properties `((upstream-name . "rpicosat")))
   (build-system r-build-system)
   (home-page
    "https://github.com/dirkschumacher/rpicosat")
   (synopsis
    "R Bindings for the 'PicoSAT' SAT Solver")
   (description
    "Bindings for the 'PicoSAT' solver to solve Boolean satisfiability problems (SAT).  The boolean satisfiability problem asks the question if a given boolean formula can be TRUE; i.e.  does there exist an assignment of TRUE/FALSE for each variable such that the whole formula is TRUE? The package bundles 'PicoSAT' solver release 965 <http://www.fmv.jku.at/picosat/>.")
   (license license:expat)))

(define-public r-ryacas
  (package
   (name "r-ryacas")
   (version "1.1.3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "Ryacas" version))
     (sha256
      (base32
       "12vkylq06c7kqvvf662cyzhdcnpy928xsjflf1kf0nh6y00gywf8"))))
   (properties `((upstream-name . "Ryacas")))
   (build-system r-build-system)
   (propagated-inputs (list r-magrittr r-rcpp))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/r-cas/ryacas")
   (synopsis
    "R Interface to the 'Yacas' Computer Algebra System")
   (description
    "Interface to the 'yacas' computer algebra system (<http://www.yacas.org/>).")
   (license (list license:gpl2+ license:gpl3+))))

(define-public r-showimage
  (package
   (name "r-showimage")
   (version "1.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "showimage" version))
     (sha256
      (base32
       "1c0x3iqjdjsz3cdhc02b3qm2pwxjr1q2k87jwvxj9lnzzw81f1pl"))))
   (properties `((upstream-name . "showimage")))
   (build-system r-build-system)
   (propagated-inputs (list r-png))
   (home-page
    "https://github.com/r-lib/showimage#readme")
   (synopsis
    "Show an Image on an 'R' Graphics Device")
   (description
    "Sometimes it is handy to be able to view an image file on an 'R' graphics device.  This package just does that.  Currently it supports 'PNG' files.")
   (license (list license:gpl2 license:gpl3))))

(define-public r-quandl
  (package
   (name "r-quandl")
   (version "2.11.0")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "Quandl" version))
            (sha256
             (base32
              "1bsba6blbcq0my8wm0qcy0qabbgx344gjljnjwaqfrqkjcphf5xs"))))
   (properties `((upstream-name . "Quandl")))
   (build-system r-build-system)
   (propagated-inputs (list r-httr r-jsonlite r-xts r-zoo))
   (home-page "https://github.com/quandl/quandl-r")
   (synopsis "API Wrapper for Quandl.com")
   (description
    "This package provides functions for interacting directly with the Quandl API to
offer data in a number of formats usable in R, downloading a zip with all data
from a Quandl database, and the ability to search.  This R package uses the
Quandl API. For more information go to <https://docs.quandl.com>.  For more help
on the package itself go to <https://www.quandl.com/tools/r>.")
   (license license:expat)))

;; TODO: upgrade available to 1.0.0 but with many more dependencies
(define-public r-timetk
  (package
   (name "r-timetk")
   (version "0.1.2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "timetk" version))
     (sha256
      (base32
       "0dp7pjydqcdi2nn4q73nqr4i4d9n4md9vb5y3znrlf1m8m9m4k4a"))))
   (properties `((upstream-name . "timetk")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-dplyr" ,r-dplyr)
      ("r-lazyeval" ,r-lazyeval)
      ("r-lubridate" ,r-lubridate)
      ("r-padr" ,r-padr)
      ("r-purrr" ,r-purrr)
      ("r-readr" ,r-readr)
      ("r-stringi" ,r-stringi)
      ("r-tibble" ,r-tibble)
      ("r-tidyr" ,r-tidyr)
      ("r-xts" ,r-xts)
      ("r-zoo" ,r-zoo)))
   (home-page
    "https://github.com/business-science/timetk")
   (synopsis
    "A Tool Kit for Working with Time Series")
   (description
    " Get the time series index (date or date-time component), time series signature (feature extraction of date or date-time component for time series machine learning), and time series summary (summary attributes about time series).  Create future time series based on properties of existing time series index using logistic regression.  Coerce between time-based tibbles ('tbl') and 'xts', 'zoo', and 'ts'.  Methods discussed herein are commonplace in machine learning, and have been cited in various literature.  Refer to \"Calendar Effects\" in papers such as Taieb, Souhaib Ben. \"Machine learning strategies for multi-step-ahead time series forecasting.\" Universit Libre de Bruxelles, Belgium (2014): 75-86. <http://souhaib-bentaieb.com/pdf/2014_phd.pdf>.")
   (license license:gpl3+)))

(define-public r-tidyquant
  (package
   (name "r-tidyquant")
   (version "0.5.8")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "tidyquant" version))
     (sha256
      (base32
       "1wg2r2197j846kapra58z51r6xgp38g0m6rb3dvmz5awlkb9mdz8"))))
   (properties `((upstream-name . "tidyquant")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-cli" ,r-cli)
      ("r-crayon" ,r-crayon)
      ("r-dplyr" ,r-dplyr)
      ("r-ggplot2" ,r-ggplot2)
      ("r-httr" ,r-httr)
      ("r-lazyeval" ,r-lazyeval)
      ("r-lubridate" ,r-lubridate)
      ("r-magrittr" ,r-magrittr)
      ("r-performanceanalytics"
       ,r-performanceanalytics)
      ("r-purrr" ,r-purrr)
      ("r-quandl" ,r-quandl)
      ("r-quantmod" ,r-quantmod)
      ("r-rlang" ,r-rlang)
      ("r-rstudioapi" ,r-rstudioapi)
      ("r-stringr" ,r-stringr)
      ("r-tibble" ,r-tibble)
      ("r-tidyr" ,r-tidyr)
      ("r-timetk" ,r-timetk)
      ("r-ttr" ,r-ttr)
      ("r-xml2" ,r-xml2)
      ("r-xts" ,r-xts)))
   (home-page
    "https://github.com/business-science/tidyquant")
   (synopsis "Tidy Quantitative Financial Analysis")
   (description
    "Bringing financial analysis to the 'tidyverse'.  The 'tidyquant' package provides a convenient wrapper to various 'xts', 'zoo', 'quantmod', 'TTR' and 'PerformanceAnalytics' package functions and returns the objects in the tidy 'tibble' format.  The main advantage is being able to use quantitative functions with the 'tidyverse' functions including 'purrr', 'dplyr', 'tidyr', 'ggplot2', 'lubridate', etc.  See the 'tidyquant' website for more information, documentation and examples.")
   (license license:expat)))

(define-public r-alphavantager
  (package
   (name "r-alphavantager")
   (version "0.1.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "alphavantager" version))
     (sha256
      (base32
       "0rbvbws0g50z76pvkydadf97gq5yg1l03xlfqyy3yblqh59cj5s6"))))
   (properties `((upstream-name . "alphavantager")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-dplyr" ,r-dplyr)
      ("r-glue" ,r-glue)
      ("r-httr" ,r-httr)
      ("r-jsonlite" ,r-jsonlite)
      ("r-purrr" ,r-purrr)
      ("r-readr" ,r-readr)
      ("r-stringr" ,r-stringr)
      ("r-tibble" ,r-tibble)
      ("r-tidyr" ,r-tidyr)
      ("r-timetk" ,r-timetk)))
   (home-page
    "https://github.com/business-science/alphavantager")
   (synopsis
    "Lightweight R Interface to the Alpha Vantage API")
   (description
    " Alpha Vantage has free historical financial information.  All you need to do is get a free API key at <https://www.alphavantage.co>.  Then you can use the R interface to retrieve free equity information.  Refer to the Alpha Vantage website for more information.")
   (license license:gpl3+)))

(define-public r-padr
  (package
   (name "r-padr")
   (version "0.5.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "padr" version))
     (sha256
      (base32
       "1lckaizlffgdzs7rhlkafam2clqkn3130r4fdnanm2bvd6b0z220"))))
   (properties `((upstream-name . "padr")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-dplyr" ,r-dplyr)
      ("r-lubridate" ,r-lubridate)
      ("r-rcpp" ,r-rcpp)
      ("r-rlang" ,r-rlang)))
   (home-page "https://github.com/EdwinTh/padr")
   (synopsis
    "Quickly Get Datetime Data Ready for Analysis")
   (description
    "Transforms datetime data into a format ready for analysis.  It offers two core functionalities; aggregating data to a higher level interval (thicken) and imputing records where observations were absent (pad).")
   (license license:expat)))

(define-public r-rgenoud
  (package
   (name "r-rgenoud")
   (version "5.8-3.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "rgenoud" version))
     (sha256
      (base32
       "0p93wf6ghgz2nifxbscb6bhahh5jd2ba7nh1c2mb6fmbxnsi3swv"))))
   (properties `((upstream-name . "rgenoud")))
   (build-system r-build-system)
   (home-page "http://sekhon.berkeley.edu/rgenoud/")
   (synopsis
    "R Version of GENetic Optimization Using Derivatives")
   (description
    "This package provides a genetic algorithm plus derivative optimizer.")
   (license license:gpl3)))

(define-public r-edgarwebr
  (package
   (name "r-edgarwebr")
   (version "1.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "edgarWebR" version))
     (sha256
      (base32
       "0rnf3s5d5sclvd4y195mfkrazg4qvpyyrgl6jcqikygbcz8hg3j8"))))
   (properties `((upstream-name . "edgarWebR")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-httr" ,r-httr) ("r-xml2" ,r-xml2)))
   (home-page
    "https://mwaldstein.github.io/edgarWebR")
   (synopsis "SEC Filings Access")
   (description
    "This package provides a set of methods to access and parse live filing information from the U.S.  Securities and Exchange Commission (SEC - <https://sec.gov>) including company and fund filings along with all associated metadata.")
   (license license:expat)))

(define-public r-blscraper
  (package
   (name "r-blscraper")
   (version "3.1.6")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "blscrapeR" version))
     (sha256
      (base32
       "0p1lwl3zlah0x9jwvd7qmh8q3ynqz48wj6xvzyj7c9rl0iq2ws5x"))))
   (properties `((upstream-name . "blscrapeR")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-dplyr" ,r-dplyr)
      ("r-ggplot2" ,r-ggplot2)
      ("r-httr" ,r-httr)
      ("r-jsonlite" ,r-jsonlite)
      ("r-magrittr" ,r-magrittr)
      ("r-purrr" ,r-purrr)
      ("r-stringr" ,r-stringr)
      ("r-tibble" ,r-tibble)))
   (home-page
    "https://github.com/keberwein/blscrapeR")
   (synopsis
    "An API Wrapper for the Bureau of Labor Statistics (BLS)")
   (description
    "Scrapes various data from <https://www.bls.gov/>.  The U.S.  Bureau of Labor Statistics is the statistical branch of the United States Department of Labor.  The package has additional functions to help parse, analyze and visualize the data.")
   (license license:expat)))

(define-public r-ggdark
  (package
   (name "r-ggdark")
   (version "0.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ggdark" version))
     (sha256
      (base32
       "1w93g2j4g45x9s841v9zi18lxzda81ipa13fajqc6p9xk8frvgrf"))))
   (properties `((upstream-name . "ggdark")))
   (build-system r-build-system)
   (propagated-inputs `(("r-ggplot2" ,r-ggplot2)))
   (home-page
    "https://cran.r-project.org/web/packages/ggdark")
   (synopsis "Dark Mode for 'ggplot2' Themes")
   (description
    "Activate dark mode on your favorite 'ggplot2' theme with dark_mode() or use the dark versions of 'ggplot2' themes, including dark_theme_gray(), dark_theme_minimal(), and others.  When a dark theme is applied, all geom color and geom fill defaults are changed to make them visible against a dark background.  To restore the defaults to their original values, use invert_geom_defaults().")
   (license license:expat)))

(define-public r-gwidgets
  (package
   (name "r-gwidgets")
   (version "0.0-54.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gWidgets" version))
     (sha256
      (base32
       "1vwwjpi4lbgzw3fw3j9cccs9qhqa11v5hvq4hv5px373dla8pcn2"))))
   (properties `((upstream-name . "gWidgets")))
   (build-system r-build-system)
   (home-page
    "https://r-forge.r-project.org/R/?group_id=761")
   (synopsis
    "gWidgets API for Building Toolkit-Independent, Interactive GUIs")
   (description
    "This package provides a toolkit-independent API for building interactive GUIs.  At least one of the 'gWidgetsXXX packages', such as gWidgetstcltk, needs to be installed.  Some icons are on loan from the scigraphica project <http://scigraphica.sourceforge.net>.")
   (license license:gpl2+)))

(define-public r-cairodevice
  (package
   (name "r-cairodevice")
   (version "2.28")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "cairoDevice" version))
     (sha256
      (base32
       "14885gpmhdzmxbz3q59s5sd3kjkyi5yf9lpnjwmvc482n8ix4lpq"))))
   (properties `((upstream-name . "cairoDevice")))
   (build-system r-build-system)
   (inputs `(("cairo" ,cairo)
	     ("gtk+2.0", gtk+-2)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (home-page
    "https://cran.r-project.org/web/packages/cairoDevice")
   (synopsis
    "Embeddable Cairo Graphics Device Driver")
   (description
    "This device uses Cairo and GTK to draw to the screen, file (png, svg, pdf, and ps) or memory (arbitrary GdkDrawable or Cairo context).  The screen device may be embedded into RGtk2 interfaces and supports all interactive features of other graphics devices, including getGraphicsEvent().")
   (license (list license:gpl2+ license:gpl3+))))

(define-public r-hunspell
  (package
   (name "r-hunspell")
   (version "3.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "hunspell" version))
     (sha256
      (base32
       "0mwqw5p0ph083plm2hr2hqr50bjg2dw862dpsfm4l2fgyy3rryq1"))))
   (properties `((upstream-name . "hunspell")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-digest" ,r-digest) ("r-rcpp" ,r-rcpp)))
   (home-page
    "https://github.com/ropensci/hunspell#readmehttps://hunspell.github.io")
   (synopsis
    "High-Performance Stemmer, Tokenizer, and Spell Checker")
   (description
    "Low level spell checker and morphological analyzer based on the famous 'hunspell' library <https://hunspell.github.io>.  The package can analyze or check individual words as well as parse text, latex, html or xml documents.  For a more user-friendly interface use the 'spelling' package which builds on this package to automate checking of files, documentation and vignettes in all common formats.")
   (license license:gpl2+)))

(define-public r-rgtk2
  (package
   (name "r-rgtk2")
   (version "2.20.36")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "RGtk2" version))
     (sha256
      (base32
       "1vzb3wklm2iwhvggalalccr06ngx6zgv3bxjb8c3rsg9va82bzyp"))))
   (properties `((upstream-name . "RGtk2")))
   (build-system r-build-system)
   (inputs
    `(("atk" ,atk)
      ("cairo" ,cairo)
      ("glib" ,glib)
      ("gtk+-2.0" ,gtk+-2)
      ("pango" ,pango)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (home-page "http://www.ggobi.org/rgtk2")
   (synopsis "R Bindings for Gtk 2.8.0 and Above")
   (description
    "Facilities in the R language for programming graphical interfaces using Gtk, the Gimp Tool Kit.")
   (license (list license:gpl2+ license:gpl3+))))

(define-public r-gwidgetsrgtk2
  (package
   (name "r-gwidgetsrgtk2")
   (version "0.0-86")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gWidgetsRGtk2" version))
     (sha256
      (base32
       "0b1xqffbzxlaaaqf1vzfl0a7b6wnnslsp8v5fbxblv7w951rsc4m"))))
   (properties `((upstream-name . "gWidgetsRGtk2")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-cairodevice" ,r-cairodevice)
      ("r-gwidgets" ,r-gwidgets)
      ("r-rgtk2" ,r-rgtk2)))
   (home-page
    "https://cran.r-project.org/web/packages/gWidgetsRGtk2")
   (synopsis
    "Toolkit Implementation of gWidgets for RGtk2")
   (description
    "Port of the gWidgets API to the RGtk2 toolkit.")
   (license license:gpl2+)))

(define-public r-rqda
  (package
   (name "r-rqda")
   (version "0.3-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "RQDA" version))
     (sha256
      (base32
       "1kqax4m4n5h52gi0jaq5cvdh1dgl0bvn420dbws9h5vrabbw1c1w"))))
   (properties `((upstream-name . "RQDA")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-dbi" ,r-dbi)
      ("r-gwidgets" ,r-gwidgets)
      ("r-gwidgetsrgtk2" ,r-gwidgetsrgtk2)
      ("r-igraph" ,r-igraph)
      ("r-rgtk2" ,r-rgtk2)
      ("r-rsqlite" ,r-rsqlite)))
   (home-page "http://rqda.r-forge.r-project.org")
   (synopsis "Qualitative Data Analysis")
   (description
    "Software for qualitative text analysis (Kuckartz, 2014, <doi:10.4135/9781446288719>).  Current version only supports plain text, but it can import PDF highlights if package 'rjpod' (<https://r-forge.r-project.org/projects/rqda/>) is installed.")
   (license license:bsd-3)))

(define-public r-tokenizers
  (package
   (name "r-tokenizers")
   (version "0.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "tokenizers" version))
     (sha256
      (base32
       "006xf1vdrmp9skhpss9ldhmk4cwqk512cjp1pxm2gxfybpf7qq98"))))
   (properties `((upstream-name . "tokenizers")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-rcpp" ,r-rcpp)
      ("r-snowballc" ,r-snowballc)
      ("r-stringi" ,r-stringi)))
   (home-page
    "https://lincolnmullen.com/software/tokenizers/")
   (synopsis
    "Fast, Consistent Tokenization of Natural Language Text")
   (description
    "Convert natural language text into tokens.  Includes tokenizers for shingled n-grams, skip n-grams, words, word stems, sentences, paragraphs, characters, shingled characters, lines, tweets, Penn Treebank, regular expressions, as well as functions for counting characters, words, and sentences, and a function for splitting longer texts into separate documents, each with the same number of words.  The tokenizers have a consistent interface, and the package is built on the 'stringi' and 'Rcpp' packages for  fast yet correct tokenization in 'UTF-8'.")
   (license license:expat)))

(define-public r-cpcg
  (package
   (name "r-cpcg")
   (version "1.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "cPCG" version))
     (sha256
      (base32
       "1pfbsv2rcjsryn6nr56a7i4yb7k0m3gdfn4q9l1kpzhmv9lic7m1"))))
   (properties `((upstream-name . "cPCG")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)))
   (home-page
    "https://cran.r-project.org/web/packages/cPCG")
   (synopsis
    "Efficient and Customized Preconditioned Conjugate Gradient Method for Solving System of Linear Equations")
   (description
    "Solves system of linear equations using (preconditioned) conjugate gradient algorithm, with improved efficiency using Armadillo templated 'C++' linear algebra library, and flexibility for user-specified preconditioning method.  Please check <https://github.com/styvon/cPCG> for latest updates.")
   (license license:gpl2+)))

(define-public r-crossdes
  (package
   (name "r-crossdes")
   (version "1.1-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "crossdes" version))
     (sha256
      (base32
       "1d7lv3ibq1rwxx8kc3ia6l9dbz2dxdd5pnf2vhhjmwm448iamcfd"))))
   (properties `((upstream-name . "crossdes")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-algdesign" ,r-algdesign)
      ("r-gtools" ,r-gtools)))
   (home-page
    "https://cran.r-project.org/web/packages/crossdes")
   (synopsis "Construction of Crossover Designs")
   (description
    "Contains functions for the construction of carryover balanced crossover designs.  In addition contains functions to check given designs for balance.")
   (license license:gpl2)))

(define-public r-rgeos
  (package
   (name "r-rgeos")
   (version "0.5-2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "rgeos" version))
     (sha256
      (base32
       "0ajrrxfwfp184c6rnby3dq54hzp3rrx3px72dn7gbdn6c4li2bqz"))))
   (properties `((upstream-name . "rgeos")))
   (build-system r-build-system)
   (propagated-inputs `(("r-sp" ,r-sp)
			("geos" ,geos)))
   (home-page
    "https://cran.r-project.org/web/packages/rgeos")
   (synopsis
    "Interface to Geometry Engine - Open Source ('GEOS')")
   (description
    "Interface to Geometry Engine - Open Source ('GEOS') using the C 'API' for topology operations on geometries.  The 'GEOS' library is external to the package, and, when installing the package from source, must be correctly installed first.  Windows and Mac Intel OS X binaries are provided on 'CRAN'. ('rgeos' >= 0.5-1): Up to and including 'GEOS' 3.7.1, topological operations succeeded with some invalid geometries for which the same operations fail from and including 'GEOS' 3.7.2.  The 'checkValidity=' argument defaults and structure have been changed, from default FALSE to integer default '0L' for 'GEOS' < 3.7.2 (no check), '1L' 'GEOS' >= 3.7.2 (check and warn).  A value of '2L' is also provided that may be used, assigned globally using 'set_RGEOS_CheckValidity(2L)', or locally using the 'checkValidity=2L' argument, to attempt zero-width buffer repair if invalid geometries are found.  The previous default (FALSE, now '0L') is fastest and used for 'GEOS' < 3.7.2, but will not warn users of possible problems before the failure of topological operations that previously succeeded.")
   (license license:gpl2+)))

(define-public r-spsurvey
  (package
   (name "r-spsurvey")
   (version "4.1.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "spsurvey" version))
     (sha256
      (base32
       "0ckr2lciqax391g6q6zqsq1bmjbcpp1k8ndzr754vslqn5cibffj"))))
   (properties `((upstream-name . "spsurvey")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-crossdes" ,r-crossdes)
      ("r-deldir" ,r-deldir)
      ("r-foreign" ,r-foreign)
      ("r-hmisc" ,r-hmisc)
      ("r-mass" ,r-mass)
      ("r-rgeos" ,r-rgeos)
      ("r-sf" ,r-sf)
      ("r-sp" ,r-sp)))
   (home-page
    "https://cran.r-project.org/web/packages/spsurvey")
   (synopsis "Spatial Survey Design and Analysis")
   (description
    "These functions provide procedures for selecting sites for spatial surveys using spatially balanced algorithms applied to discrete points, linear networks, or polygons.  The probability survey designs available include independent random samples, stratified random samples, and unequal probability random samples (categorical or probability proportional to size).  Design-based estimation based on the results from surveys is available for estimating totals, means, quantiles, CDFs, and linear models.  The analyses rely on package survey for most results.  Variance estimation options include a local neighborhood variance estimator that is appropriate for spatially-balanced survey designs.  A reference for the survey design portion of the package is: D.  L.  Stevens, Jr.  and A.  R.  Olsen (2004), \"Spatially-balanced sampling of natural resources.\", Journal of the American Statistical Association 99(465): 262-278, <DOI:10.1198/016214504000000250>.  Additional helpful references for this package are A.  R.  Olsen, T.  M.  Kincaid, and Q.  Payton (2012) and T.  M.  Kincaid and A.  R.  Olsen (2012), both of which are chapters in the book \"Design and Analysis of Long-Term Ecological Monitoring Studies\" (R.  A.  Gitzen, J.  J.  Millspaugh, A.  B.  Cooper, and D.  S.  Licht (eds.), Cambridge University Press, New York, <Online ISBN:9781139022422>).")
   (license license:gpl3+)))

(define-public r-sdraw
  (package
   (name "r-sdraw")
   (version "2.1.8")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "SDraw" version))
     (sha256
      (base32
       "05yn0mqdv5a3zyvd6jhmkjh4w06ry8zy0libr56jmp8fcnninvyw"))))
   (properties `((upstream-name . "SDraw")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-deldir" ,r-deldir)
      ("r-rgeos" ,r-rgeos)
      ("r-sp" ,r-sp)
      ("r-spsurvey" ,r-spsurvey)))
   (home-page
    "https://github.com/tmcd82070/SDraw/wiki/SDraw")
   (synopsis
    "Spatially Balanced Samples of Spatial Objects")
   (description
    "Routines for drawing samples from spatial objects, focused on spatially balanced algorithms.  Draws Halton Iterative Partition (HIP) (Robertson et al., 2018; <doi:10.1007/s10651-018-0406-6>), Balanced Acceptance Samples (BAS) (Robertson et al., 2013; <doi:10.1111/biom.12059>), Generalized Random Tessellation Stratified (GRTS) (Stevens and Olsen, 2004; <doi:10.1198/016214504000000250>), Simple Systematic Samples (SSS) and Simple Random Samples (SRS) from point, line, and polygon resources.  Frames are 'SpatialPoints', 'SpatialLines', or 'SpatialPolygons' objects from package 'sp'.")
   (license license:gpl3)))

(define-public r-netrankr
  (package
   (name "r-netrankr")
   (version "0.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "netrankr" version))
     (sha256
      (base32
       "0csgi09ijp7ygwk7x057l9xjrzyhr7sav8hamh0p6i72l6719ssl"))))
   (properties `((upstream-name . "netrankr")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-igraph" ,r-igraph)
      ("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)))
   (home-page
    "https://schochastics.github.io/netrankr")
   (synopsis
    "Analyzing Partial Rankings in Networks")
   (description
    "Implements methods for centrality related analyses of networks.  While the package includes the possibility to build more than 20 indices, its main focus lies on index-free assessment of centrality via partial rankings obtained by neighborhood-inclusion or positional dominance.  These partial rankings can be analyzed with different methods, including probabilistic methods like computing expected node ranks and relative rank probabilities (how likely is it that a node is more central than another?).  The methodology is described in depth in the vignettes and in Schoch (2018) <doi:10.1016/j.socnet.2017.12.003>.")
   (license license:expat)))

(define-public r-netswan
  (package
   (name "r-netswan")
   (version "0.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "NetSwan" version))
     (sha256
      (base32
       "1mwdy3ahagiifj2bd1ajrafvnxzi74a1x1d3i2laf1hqpz3fbgld"))))
   (properties `((upstream-name . "NetSwan")))
   (build-system r-build-system)
   (propagated-inputs `(("r-igraph" ,r-igraph)))
   (home-page
    "https://cran.r-project.org/web/packages/NetSwan")
   (synopsis
    "Network Strengths and Weaknesses Analysis")
   (description
    "This package provides a set of functions for studying network robustness, resilience and vulnerability.")
   (license license:gpl2+)))

(define-public r-asioheaders
  (package
   (name "r-asioheaders")
   (version "1.12.2-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "AsioHeaders" version))
     (sha256
      (base32
       "0ibbw8njqq53d2gvwh604xkmkjc02zf8z30gm0a1d6cx82n0w8h8"))))
   (properties `((upstream-name . "AsioHeaders")))
   (build-system r-build-system)
   (home-page
    "https://cran.r-project.org/web/packages/AsioHeaders")
   (synopsis "'Asio' C++ Header Files")
   (description
    "'Asio' is a cross-platform C++ library for network and low-level I/O programming that provides developers with a consistent asynchronous model using a modern C++ approach.  It is also included in Boost but requires linking when used with Boost.  Standalone it can be used header-only (provided a recent compiler). 'Asio' is written and maintained by Christopher M.  Kohlhoff, and released under the 'Boost Software License', Version 1.0.")
   (license license:boost1.0)))

(define-public r-websocket
  (package
   (name "r-websocket")
   (version "1.4.1")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "websocket" version))
            (sha256
             (base32
              "1ks9cyj39jnb0rkx2ii3ckmpl373m5f8sz0i4q3gk7kkv3js07r8"))))
   (properties `((upstream-name . "websocket")))
   (build-system r-build-system)
   (inputs (list openssl zlib))
   (propagated-inputs (list r-asioheaders r-cpp11 r-later r-r6 libressl))
   (native-inputs (list pkg-config r-knitr))
   (home-page "https://cran.r-project.org/package=websocket")
   (synopsis "'WebSocket' Client Library")
   (description
    "This package provides a WebSocket client interface for R. WebSocket is a
protocol for low-overhead real-time communication:
<https://en.wikipedia.org/wiki/WebSocket>.")
   (license license:gpl2)))

(define-public r-pagedown
  (package
   (name "r-pagedown")
   (version "0.8")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "pagedown" version))
     (sha256
      (base32
       "0hrr8y94wy26sw21rjqnhw6gwnxkyknbkbj84lva0876nsajhdjz"))))
   (properties `((upstream-name . "pagedown")))
   (build-system r-build-system)
   (inputs `(("pandoc" ,ghc-pandoc)))
   (propagated-inputs
    `(("r-bookdown" ,r-bookdown)
      ("r-htmltools" ,r-htmltools)
      ("r-httpuv" ,r-httpuv)
      ("r-jsonlite" ,r-jsonlite)
      ("r-later" ,r-later)
      ("r-processx" ,r-processx)
      ("r-rmarkdown" ,r-rmarkdown)
      ("r-servr" ,r-servr)
      ("r-websocket" ,r-websocket)
      ("r-xfun" ,r-xfun)))
   (home-page "https://github.com/rstudio/pagedown")
   (synopsis
    "Paginate the HTML Output of R Markdown with CSS for Print")
   (description
    "Use the paged media properties in CSS and the JavaScript library 'paged.js' to split the content of an HTML document into discrete pages.  Each page can have its page size, page numbers, margin boxes, and running headers, etc.  Applications of this package include books, letters, reports, papers, business cards, resumes, and posters.")
   (license license:expat)))


(define-public r-bnlearn
  (package
   (name "r-bnlearn")
   (version "4.5")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "bnlearn" version))
     (sha256
      (base32
       "0y452cw1dxm1cik4na1zq6g0zv5ybiwc1cq9sdashq1jacjpc158"))))
   (properties `((upstream-name . "bnlearn")))
   (build-system r-build-system)
   (home-page "http://www.bnlearn.com/")
   (synopsis
    "Bayesian Network Structure Learning, Parameter Learning and Inference")
   (description
    "Bayesian network structure learning, parameter learning and inference.  This package implements constraint-based (PC, GS, IAMB, Inter-IAMB, Fast-IAMB, MMPC, Hiton-PC, HPC), pairwise (ARACNE and Chow-Liu), score-based (Hill-Climbing and Tabu Search) and hybrid (MMHC, RSMAX2, H2PC) structure learning algorithms for discrete, Gaussian and conditional Gaussian networks, along with many score functions and conditional independence tests.  The Naive Bayes and the Tree-Augmented Naive Bayes (TAN) classifiers are also implemented.  Some utility functions (model comparison and manipulation, random data generation, arc orientation testing, simple and advanced plots) are included, as well as support for parameter estimation (maximum likelihood and Bayesian) and inference, conditional probability queries, cross-validation, bootstrap and model averaging.  Development snapshots with the latest bugfixes are available from <http://www.bnlearn.com>.")
   (license license:gpl2+)))

(define-public r-glmgraph
  (package
   (name "r-glmgraph")
   (version "1.0.3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "glmgraph" version))
     (sha256
      (base32
       "16sq6i7kbw20nvwikpa02z3pb7wqw3270j6ss7f8sgf548skhmx0"))))
   (properties `((upstream-name . "glmgraph")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)))
   (home-page
    "https://cran.r-project.org/web/packages/glmgraph")
   (synopsis
    "Graph-Constrained Regularization for Sparse Generalized Linear Models")
   (description
    "We propose to use sparse regression model to achieve variable selection while accounting for graph-constraints among coefficients.  Different linear combination of a sparsity penalty(L1) and a smoothness(MCP) penalty has been used, which induces both sparsity of the solution and certain smoothness on the linear coefficients.")
   (license license:gpl2)))

(define-public r-visnetwork
  (package
    (name "r-visnetwork")
    (version "2.0.9")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "visNetwork" version))
       (sha256
        (base32
	 "0854r9znpjd9iy6j5bgrn20vj13dhp606gs3b6iy0rhym71ks2sy"))))
    (properties `((upstream-name . "visNetwork")))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-htmltools" ,r-htmltools)
       ("r-htmlwidgets" ,r-htmlwidgets)
       ("r-jsonlite" ,r-jsonlite)
       ("r-magrittr" ,r-magrittr)))
    (native-inputs `(("r-knitr" ,r-knitr)))
    (home-page
     "http://datastorm-open.github.io/visNetwork/")
    (synopsis
     "Network Visualization using 'vis.js' Library")
    (description
     "This package provides an R interface to the 'vis.js' JavaScript charting library.  It allows an interactive visualization of networks.")
    (license license:expat)))

(define-public r-labelvector
  (package
   (name "r-labelvector")
   (version "0.1.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "labelVector" version))
     (sha256
      (base32
       "156066jbsmqal1q4xwwkyrqfzq0as6r2add0x240y21zak6i8x9q"))))
   (properties `((upstream-name . "labelVector")))
   (build-system r-build-system)
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "https://cran.r-project.org/web/packages/labelVector")
   (synopsis "Label Attributes for Atomic Vectors")
   (description
    "Labels are a common construct in statistical software providing a human readable description of a variable.  While variable names are succinct, quick to type, and follow a language's naming conventions, labels may be more illustrative and may use plain text and spaces.  R does not provide native support for labels.  Some packages, however, have made this feature available.  Most notably, the 'Hmisc' package provides labelling methods for a number of different object.  Due to design decisions, these methods are not all exported, and so are unavailable for use in package development.  The 'labelVector' package supports labels for atomic vectors in a light-weight design that is suitable for use in other packages.")
   (license license:expat)))

(define-public r-pixiedust
  (package
   (name "r-pixiedust")
   (version "0.8.6")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "pixiedust" version))
     (sha256
      (base32
       "1dajiblpm51szndz026lmwh6swx8f9f03s6md26d84awcx0q1dpc"))))
   (properties `((upstream-name . "pixiedust")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-broom" ,r-broom)
      ("r-checkmate" ,r-checkmate)
      ("r-dplyr" ,r-dplyr)
      ("r-htmltools" ,r-htmltools)
      ("r-knitr" ,r-knitr)
      ("r-labelvector" ,r-labelvector)
      ("r-magrittr" ,r-magrittr)
      ("r-scales" ,r-scales)
      ("r-tidyr" ,r-tidyr)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "https://github.com/nutterb/pixiedust")
   (synopsis
    "Tables so Beautifully Fine-Tuned You Will Believe It's Magic")
   (description
    "The introduction of the 'broom' package has made converting model objects into data frames as simple as a single function.  While the 'broom' package focuses on providing tidy data frames that can be used in advanced analysis, it deliberately stops short of providing functionality for reporting models in publication-ready tables. 'pixiedust' provides this functionality with a programming interface intended to be similar to 'ggplot2's system of layers with fine tuned control over each cell of the table.  Options for output include printing to the console and to the common markdown formats (markdown, HTML, and LaTeX).  With a little 'pixiedust' (and happy thoughts) tables can really fly.")
   (license license:gpl2+)))

(define-public r-catnet
  (package
   (name "r-catnet")
   (version "1.15.7")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "catnet" version))
     (sha256
      (base32
       "015qh9k0jj5plp777j3kl20601qiwadkaqrdb0483h3gwqwy828g"))))
   (properties `((upstream-name . "catnet")))
   (build-system r-build-system)
   (home-page
    "https://cran.r-project.org/web/packages/catnet")
   (synopsis
    "Categorical Bayesian Network Inference")
   (description
    "Structure learning and parameter estimation of discrete Bayesian networks using likelihood-based criteria.  Exhaustive search for fixed node orders and stochastic search of optimal orders via simulated annealing algorithm are implemented.")
   (license license:gpl2+)))

(define-public r-grbase
  (package
   (name "r-grbase")
   (version "1.8-6.4")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gRbase" version))
     (sha256
      (base32
       "08rp6jjpcxzzad617fycc4yq7jlkclkvfa3dvb2qj4k4g1xpl2h9"))))
   (properties `((upstream-name . "gRbase")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-graph" ,r-graph)
      ("r-igraph" ,r-igraph)
      ("r-magrittr" ,r-magrittr)
      ("r-matrix" ,r-matrix)
      ("r-rbgl" ,r-rbgl)
      ("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)
      ("r-rcppeigen" ,r-rcppeigen)
      ("r-rgraphviz" ,r-rgraphviz)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "http://people.math.aau.dk/~sorenh/software/gR/")
   (synopsis
    "A Package for Graphical Modelling in R")
   (description
    "The 'gRbase' package provides graphical modelling features used by e.g.  the packages 'gRain', 'gRim' and 'gRc'. 'gRbase' contains data sets relevant for use in connection with graphical models (in particular all data sets used in the book Graphical Models with R (2012)). 'gRbase' implements graph algorithms including (i) maximum cardinality search (for marked and unmarked graphs). (ii) moralize. (iii) triangulate. (iv) junction tree. 'gRbase' facilitates array operations, 'gRbase' implements functions for testing for conditional independence. 'gRbase' illustrates how hierarchical log-linear models may be implemented and describes concept of graphical meta data.  These features, however, are not maintained anymore and remains in 'gRbase' only because there exists a paper describing these facilities: A Common Platform for Graphical Models in R: The 'gRbase' Package, Journal of Statistical Software, Vol 14, No 17, 2005.  NOTICE  'gRbase' requires that the packages graph, 'Rgraphviz' and 'RBGL' are installed from 'bioconductor'; for installation instructions please refer to the web page given below.")
   (license license:gpl2+)))

(define-public r-grain
  (package
   (name "r-grain")
   (version "1.3-4")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gRain" version))
     (sha256
      (base32
       "0gd04qd92mnd9j9r1ln3m19dywkbx4aq979lxm0csm7d0vhbdq99"))))
   (properties `((upstream-name . "gRain")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-graph" ,r-graph)
      ("r-grbase" ,r-grbase)
      ("r-igraph" ,r-igraph)
      ("r-magrittr" ,r-magrittr)
      ("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)
      ("r-rcppeigen" ,r-rcppeigen)
      ("r-rgraphviz" ,r-rgraphviz)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "http://people.math.aau.dk/~sorenh/software/gR/")
   (synopsis "Graphical Independence Networks")
   (description
    "Probability propagation in graphical independence networks, also known as Bayesian networks or probabilistic expert systems.")
   (license license:gpl2+)))

(define-public r-grim
  (package
   (name "r-grim")
   (version "0.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gRim" version))
     (sha256
      (base32
       "0j5r4pnjmyc9z0yfs8bgp0w8w4prlqwvnxmg5pm5gz09h8kkd2qq"))))
   (properties `((upstream-name . "gRim")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-grain" ,r-grain)
      ("r-graph" ,r-graph)
      ("r-grbase" ,r-grbase)
      ("r-igraph" ,r-igraph)
      ("r-magrittr" ,r-magrittr)
      ("r-rcpp" ,r-rcpp)
      ("r-rcpparmadillo" ,r-rcpparmadillo)
      ("r-rcppeigen" ,r-rcppeigen)
      ("r-rgraphviz" ,r-rgraphviz)))
   (home-page
    "http://people.math.aau.dk/~sorenh/software/gR/")
   (synopsis "Graphical Interaction Models")
   (description
    "This package provides the following types of models: Models for for contingency tables (i.e.  log-linear models) Graphical Gaussian models for multivariate normal data (i.e.  covariance selection models) Mixed interaction models.")
   (license license:gpl2+)))

(define-public r-grc
  (package
   (name "r-grc")
   (version "0.4-3.2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gRc" version))
     (sha256
      (base32
       "0f8m83wqhmsn6p0v0msdzyy9vl900nc9ddr8y78181jxcd9mqd0d"))))
   (properties `((upstream-name . "gRc")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-grbase" ,r-grbase) ("r-mass" ,r-mass)))
   (home-page
    "http://people.math.aau.dk/~sorenh/software/gR/")
   (synopsis
    "Inference in Graphical Gaussian Models with Edge and Vertex Symmetries")
   (description
    "Estimation, model selection and other aspects of statistical inference in Graphical Gaussian models with edge and vertex symmetries (Graphical Gaussian models with colours).  Documentation about 'gRc' is provided in the paper by Hojsgaard and Lauritzen (2007, <doi:10.18637/jss.v023.i06>) and the paper by Hojsgaard and Lauritzen (2008, <doi:10.1111/j.1467-9868.2008.00666.x>).")
   (license (list license:gpl2+ license:gpl3+))))

(define-public r-bnspatial
  (package
   (name "r-bnspatial")
   (version "1.1.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "bnspatial" version))
     (sha256
      (base32
       "1drm9ia4lr80wahbbn9xrw658ppmgxm4iadwv77jz1x786dda2n1"))))
   (properties `((upstream-name . "bnspatial")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-doparallel" ,r-doparallel)
      ("r-foreach" ,r-foreach)
      ("r-grain" ,r-grain)
      ("r-grbase" ,r-grbase)
      ("r-raster" ,r-raster)
      ("r-rgdal" ,r-rgdal)
      ("r-sf" ,r-sf)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "http://github.com/dariomasante/bnspatial")
   (synopsis
    "Spatial Implementation of Bayesian Networks and Mapping")
   (description
    "Allows spatial implementation of Bayesian networks and mapping in geographical space.  It makes maps of expected value (or most likely state) given known and unknown conditions, maps of uncertainty measured as coefficient of variation or Shannon index (entropy), maps of probability associated to any states of any node of the network.  Some additional features are provided as well: parallel processing options, data discretization routines and function wrappers designed for users with minimal knowledge of the R language.  Outputs can be exported to any common GIS format.")
   (license license:gpl3)))


(define-public r-bnstruct
  (package
   (name "r-bnstruct")
   (version "1.0.6")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "bnstruct" version))
     (sha256
      (base32
       "1vnjjv8ms8hvd21zsgyl056s266a1plp7ybdmnkl2cy3n8jpdwi1"))))
   (properties `((upstream-name . "bnstruct")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-bitops" ,r-bitops)
      ("r-igraph" ,r-igraph)
      ("r-matrix" ,r-matrix)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "https://cran.r-project.org/web/packages/bnstruct")
   (synopsis
    "Bayesian Network Structure Learning from Data with Missing Values")
   (description
    "Bayesian Network Structure Learning from Data with Missing Values.  The package implements the Silander-Myllymaki complete search, the Max-Min Parents-and-Children, the Hill-Climbing, the Max-Min Hill-climbing heuristic searches, and the Structural Expectation-Maximization algorithm.  Available scoring functions are BDeu, AIC, BIC.  The package also implements methods for generating and using bootstrap samples, imputed data, inference.")
   (license license:gpl2+)))

(define-public r-gramevol
  (package
   (name "r-gramevol")
   (version "2.1-3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gramEvol" version))
     (sha256
      (base32
       "0z2511zhs2gcdkwp29n6fpafhn49h69yi56nmrhh9zc8jqj1rwpa"))))
   (properties `((upstream-name . "gramEvol")))
   (build-system r-build-system)
   (home-page
    "https://github.com/fnoorian/gramEvol/")
   (synopsis "Grammatical Evolution for R")
   (description
    "This package provides a native R implementation of grammatical evolution (GE).  GE facilitates the discovery of programs that can achieve a desired goal.  This is done by performing an evolutionary optimisation over a population of R expressions generated via a user-defined context-free grammar (CFG) and cost function.")
   (license license:gpl2+)))

(define-public r-ggtern
  (package
   (name "r-ggtern")
   (version "3.3.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ggtern" version))
     (sha256
      (base32
       "12lxmql9zspglp0baqp419l0vgilrf8lyxsza142bfyvmdbhh6d3"))))
   (properties `((upstream-name . "ggtern")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-compositions" ,r-compositions)
      ("r-ggplot2" ,r-ggplot2)
      ("r-gridextra" ,r-gridextra)
      ("r-gtable" ,r-gtable)
      ("r-hexbin" ,r-hexbin)
      ("r-latex2exp" ,r-latex2exp)
      ("r-lattice" ,r-lattice)
      ("r-mass" ,r-mass)
      ("r-plyr" ,r-plyr)
      ("r-proto" ,r-proto)
      ("r-scales" ,r-scales)))
   (home-page "http://www.ggtern.com")
   (synopsis
    "An Extension to 'ggplot2', for the Creation of Ternary Diagrams")
   (description
    "Extends the functionality of 'ggplot2', providing the capability to plot ternary diagrams for (subset of) the 'ggplot2' geometries.  Additionally, 'ggtern' has implemented several NEW geometries which are unavailable to the standard 'ggplot2' release.  For further examples and documentation, please proceed to the 'ggtern' website.")
   (license license:gpl2)))

(define-public r-missmda
  (package
   (name "r-missmda")
   (version "1.16")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "missMDA" version))
     (sha256
      (base32
       "0c5a9ygvvx2qi9hgbwnhl4hcljy2b4hi2x3py2qyay9bp0zvchxm"))))
   (properties `((upstream-name . "missMDA")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-doparallel" ,r-doparallel)
      ("r-factominer" ,r-factominer)
      ("r-foreach" ,r-foreach)
      ("r-mice" ,r-mice)
      ("r-mvtnorm" ,r-mvtnorm)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "http://factominer.free.fr/missMDA/index.html")
   (synopsis
    "Handling Missing Values with Multivariate Data Analysis")
   (description
    "Imputation of incomplete continuous or categorical datasets; Missing values are imputed with a principal component analysis (PCA), a multiple correspondence analysis (MCA) model or a multiple factor analysis (MFA) model; Perform multiple imputation with and in PCA or MCA.")
   (license license:gpl2+)))

(define-public r-anytime
  (package
   (name "r-anytime")
   (version "0.3.7")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "anytime" version))
     (sha256
      (base32
       "0vkckxaq1ga73iszwr4lyf12c1cann1w9lhflz4p3xdgx468fzb9"))))
   (properties `((upstream-name . "anytime")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-bh" ,r-bh) ("r-rcpp" ,r-rcpp)))
   (home-page
    "http://dirk.eddelbuettel.com/code/anytime.html")
   (synopsis
    "Anything to 'POSIXct' or 'Date' Converter")
   (description
    "Convert input in any one of character, integer, numeric, factor, or ordered type into 'POSIXct' (or 'Date') objects, using one of a number of predefined formats, and relying on Boost facilities for date and time parsing.")
   (license license:gpl2+)))

(define-public r-tsibble
  (package
   (name "r-tsibble")
   (version "0.8.6")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "tsibble" version))
     (sha256
      (base32
       "1vm4rwfflrc87xj6sn8xa0lqdvs1k4hviar0glm9v0dhbk81bbbj"))))
   (properties `((upstream-name . "tsibble")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-anytime" ,r-anytime)
      ("r-dplyr" ,r-dplyr)
      ("r-lifecycle" ,r-lifecycle)
      ("r-lubridate" ,r-lubridate)
      ("r-purrr" ,r-purrr)
      ("r-rlang" ,r-rlang)
      ("r-tibble" ,r-tibble)
      ("r-tidyselect" ,r-tidyselect)
      ("r-vctrs" ,r-vctrs)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page "https://tsibble.tidyverts.org")
   (synopsis "Tidy Temporal Data Frames and Tools")
   (description
    "This package provides a 'tbl_ts' class (the 'tsibble') for temporal data in an data- and model-oriented format.  The 'tsibble' provides tools to easily manipulate and analyse temporal data, such as filling in time gaps and aggregating over calendar periods.")
   (license license:gpl3)))

(define-public r-qualv
  (package
   (name "r-qualv")
   (version "0.3-3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "qualV" version))
     (sha256
      (base32
       "1yyqk223ydcc0125gsn33a4mcdp8bd76fpn8kj9bfz9g78b8dqmx"))))
   (properties `((upstream-name . "qualV")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-kernsmooth" ,r-kernsmooth)))
   (home-page "http://qualV.R-Forge.R-Project.org/")
   (synopsis "Qualitative Validation Methods")
   (description
    "Qualitative methods for the validation of dynamic models.  It contains (i) an orthogonal set of deviance measures for absolute, relative and ordinal scale and (ii) approaches accounting for time shifts.  The first approach transforms time to take time delays and speed differences into account.  The second divides the time series into interval units according to their main features and finds the longest common subsequence (LCS) using a dynamic programming algorithm.")
   (license license:gpl2+)))

;; (define-public r-v8
;;   (package
;;     (name "r-v8")
;;     (version "3.0.2")
;;     (source
;;      (origin
;;        (method url-fetch)
;;        (uri (cran-uri "V8" version))
;;        (sha256
;;         (base32
;; 	 "0cwhdk77fgmlaihh2mf0iclqz7fps7s7hvsw2s65m62w9gbwfhlk"))))
;;     (properties `((upstream-name . "V8")))
;;     (build-system r-build-system)
;;     (propagated-inputs
;;      `(("r-curl" ,r-curl)
;;        ("r-jsonlite" ,r-jsonlite)
;;        ("r-rcpp" ,r-rcpp)))
;;     (native-inputs
;;      `(("r-knitr" ,r-knitr)
;;        ("v8" ,v8)))
;;     (home-page
;;      "https://jeroen.cran.dev/V8https://github.com/jeroen/v8https://v8.dev")
;;     (synopsis
;;      "Embedded JavaScript and WebAssembly Engine for R")
;;     (description
;;      "An R interface to V8: Google's open source JavaScript and WebAssembly engine.  This package can be compiled either with V8 version 6 and up, a NodeJS shared library, or the legacy 3.14/3.15 branch of V8.")
;;     (license expat)))

;; (define-public r-daff
;;   (package
;;     (name "r-daff")
;;     (version "0.3.5")
;;     (source
;;      (origin
;;        (method url-fetch)
;;        (uri (cran-uri "daff" version))
;;        (sha256
;;         (base32
;; 	 "00zcdf215m7p3xm9yv571v09znhdqacxg69hb5b5j749mp4wdsyj"))))
;;     (properties `((upstream-name . "daff")))
;;     (build-system r-build-system)
;;     (propagated-inputs
;;      `(("r-jsonlite" ,r-jsonlite)
;;        ("r-v8" ,r-v8)))
;;     (home-page "http://github.com/edwindj/daff")
;;     (synopsis
;;      "Diff, Patch and Merge for Data.frames")
;;     (description
;;      "Diff, patch and merge for data frames.  Document changes in data sets and use them to apply patches.  Changes to data can be made visible by using render_diff.  The V8 package is used to wrap the 'daff.js' JavaScript library which is included in the package.")
;;     (license expat)))

(define-public r-diffobj
  (package
   (name "r-diffobj")
   (version "0.3.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "diffobj" version))
     (sha256
      (base32
       "12kij8qjr3qv6fagyn35vrrmc5hy3g11r6gmgkgi58971cylrj6m"))))
   (properties `((upstream-name . "diffobj")))
   (build-system r-build-system)
   (propagated-inputs `(("r-crayon" ,r-crayon)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page "https://github.com/brodieG/diffobj")
   (synopsis "Diffs for R Objects")
   (description
    "Generate a colorized diff of two R objects for an intuitive visualization of their differences.")
   (license license:gpl2+)))

(define-public r-hashr
  (package
   (name "r-hashr")
   (version "0.1.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "hashr" version))
     (sha256
      (base32
       "1ri2zz2l1rrc1qmpqamzw21d9y06c7yb3wr60izw81l8z4mmyc3a"))))
   (properties `((upstream-name . "hashr")))
   (build-system r-build-system)
   (home-page
    "https://github.com/markvanderloo/hashr")
   (synopsis "Hash R Objects to Integers Fast")
   (description
    "Apply the SuperFastHash algorithm to any R object.  Hash whole R objects or, for vectors or lists, hash R objects to obtain a set of hash values that is stored in a structure equivalent to the input.")
   (license license:gpl3)))

;; (define-public r-diagrammersvg
;;   (package
;;    (name "r-diagrammersvg")
;;    (version "0.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (cran-uri "DiagrammeRsvg" version))
;;      (sha256
;;       (base32
;;        "0j2cm1mx3zrb2k3pcrb96z2z3kws61gyyjsjjv5rqcb5lzdgi65k"))))
;;    (properties `((upstream-name . "DiagrammeRsvg")))
;;    (build-system r-build-system)
;;    (propagated-inputs `(("r-v8" ,r-v8)))
;;    (home-page
;;     "https://github.com/rich-iannone/DiagrammeRsvg")
;;    (synopsis
;;     "Export DiagrammeR Graphviz Graphs as SVG")
;;    (description
;;     "Allows for export of DiagrammeR Graphviz objects to SVG.")
;;    (license expat)))

;; (define-public r-vtree
;;   (package
;;    (name "r-vtree")
;;    (version "4.0.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (cran-uri "vtree" version))
;;      (sha256
;;       (base32
;;        "0i88ab7pjbj5s5xvykx47xxhqlk1vw3anra30wvhy6cr9774x8bg"))))
;;    (properties `((upstream-name . "vtree")))
;;    (build-system r-build-system)
;;    (propagated-inputs
;;     `(("r-diagrammer" ,r-diagrammer)
;;       ("r-diagrammersvg" ,r-diagrammersvg)
;;       ("r-htmlwidgets" ,r-htmlwidgets)
;;       ("r-rsvg" ,r-rsvg)
;;       ("r-shiny" ,r-shiny)))
;;    (native-inputs `(("r-knitr" ,r-knitr)))
;;    (home-page
;;     "https://cran.r-project.org/web/packages/vtree")
;;    (synopsis
;;     "Display Information About Nested Subsets of a Data Frame")
;;    (description
;;     "This package provides a tool for calculating and drawing \"variable trees\".  Variable trees display information about hierarchical subsets of a data frame defined by values of categorical variables.")
;;    (license gpl3)))

(define-public r-proj4
  (package
   (name "r-proj4")
   (version "1.0-11")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "proj4" version))
            (sha256
             (base32
              "07fil52jswbg2h807cd82m2wlm5j2fb891ifri9ms037099qdwf5"))))
   (properties `((upstream-name . "proj4")))
   (build-system r-build-system)
   (inputs (list zlib proj))
   (native-inputs (list pkg-config))
   (home-page "http://www.rforge.net/proj4/")
   (synopsis
    "A simple interface to the PROJ.4 cartographic projections library")
   (description
    "This package provides a simple interface to lat/long projection and datum
transformation of the PROJ.4 cartographic projections library.  It allows
transformation of geographic coordinates from one projection and/or datum to
another.")
   (license license:gpl2)))

(define-public r-proj
  (package
   (name "r-proj")
   (version "0.4.0")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "PROJ" version))
            (sha256
             (base32
              "1rqw28qqcyzla3ssif9jghvv0nyms9riabj2lxhlx1l3rbz0rsfx"))))
   (properties `((upstream-name . "PROJ")))
   (build-system r-build-system)
   (native-inputs (list r-knitr))
   (home-page "https://github.com/hypertidy/PROJ")
   (synopsis "Generic Coordinate System Transformations Using 'PROJ'")
   (description
    "Currently non-operational, a harmless wrapper to allow package 'reproj' to
install and function while relying on the 'proj4' package.")
   (license license:gpl3)))

(define-public r-crsmeta
  (package
   (name "r-crsmeta")
   (version "0.3.0")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "crsmeta" version))
            (sha256
             (base32
              "19v0bq80cma74jjl0k42pmlpd5jfv6b0hr7j2iq0d47h50pvrd02"))))
   (properties `((upstream-name . "crsmeta")))
   (build-system r-build-system)
   (home-page "https://github.com/hypertidy/crsmeta")
   (synopsis "Extract Coordinate System Metadata")
   (description
    "Obtain coordinate system metadata from various data formats.  There are
functions to extract a 'CRS' (coordinate reference system,
<https://en.wikipedia.org/wiki/Spatial_reference_system>) in 'EPSG' (European
Petroleum Survey Group, <http://www.epsg.org/>), 'PROJ4' <https://proj.org/>, or
'WKT2' (Well-Known Text 2,
<http://docs.opengeospatial.org/is/12-063r5/12-063r5.html>) forms.  This is
purely for getting simple metadata from in-memory formats, please use other
tools for out of memory data sources.")
   (license license:gpl3)))

(define-public r-reproj
  (package
   (name "r-reproj")
   (version "0.4.2")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "reproj" version))
            (sha256
             (base32
              "0lxd1vsxcf0r96jkl5jrh3kyklf90wcs4a27cs6ck93x1cxjnnni"))))
   (properties `((upstream-name . "reproj")))
   (build-system r-build-system)
   (inputs (list proj))
   (propagated-inputs (list r-crsmeta r-proj r-proj4))
   (home-page "https://github.com/hypertidy/reproj/")
   (synopsis "Coordinate System Transformations for Generic Map Data")
   (description
    "Transform coordinates from a specified source to a specified target map
projection.  This uses the 'PROJ' library directly, by wrapping the 'PROJ'
package (if functional), otherwise the 'proj4' package.  The 'reproj()' function
is generic, methods may be added to remove the need for an explicit source
definition.  If 'proj4' is in use 'reproj()' handles the requirement for
conversion of angular units where necessary.  This is for use primarily to
transform generic data formats and direct leverage of the underlying 'PROJ'
library. (There are transformations that aren't possible with 'PROJ' and that
are provided by the 'GDAL' library, a limitation which users of this package
should be aware of.) The 'PROJ' library is available at <https://proj.org/>.")
   (license license:gpl3)))

(define-public r-goftest
  (package
   (name "r-goftest")
   (version "1.2-2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "goftest" version))
     (sha256
      (base32
       "0ivnkqhv5xgiv05dm648nngacymd8x8g0fyppv3bc0mhcqk9k5z4"))))
   (properties `((upstream-name . "goftest")))
   (build-system r-build-system)
   (home-page
    "https://github.com/baddstats/goftest")
   (synopsis
    "Classical Goodness-of-Fit Tests for Univariate Distributions")
   (description
    "Cramer-Von Mises and Anderson-Darling tests of goodness-of-fit for continuous univariate distributions, using efficient algorithms.")
   (license license:gpl2+)))

(define-public r-dequer
  (package
   (name "r-dequer")
   (version "2.0-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "dequer" version))
     (sha256
      (base32
       "04kzlff8xa733qkkx0gacgig7d4l7yvgqmzva0mj8di12byh214p"))))
   (properties `((upstream-name . "dequer")))
   (build-system r-build-system)
   (home-page
    "https://github.com/wrathematics/dequer")
   (synopsis "Stacks, Queues, and 'Deques' for R")
   (description
    "Queues, stacks, and 'deques' are list-like, abstract data types.  These are meant to be very cheap to \"grow\", or insert new objects into.  A typical use case involves storing data in a list in a streaming fashion, when you do not necessarily know how may elements need to be stored.  Unlike R's lists, the new data structures provided here are not necessarily stored contiguously, making insertions and deletions at the front/end of the structure much faster.  The underlying implementation is new and uses a head/tail doubly linked list; thus, we do not rely on R's environments or hashing.  To avoid unnecessary data copying, most operations on these data structures are performed via side-effects.")
   (license license:expat)))

(define-public r-randtests
  (package
   (name "r-randtests")
   (version "1.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "randtests" version))
     (sha256
      (base32
       "03z3kxl4x0l91dsv65ld9kgc58z82ld1f4lk18i18dpvwcgkqk82"))))
   (properties `((upstream-name . "randtests")))
   (build-system r-build-system)
   (home-page
    "https://cran.r-project.org/web/packages/randtests")
   (synopsis "Testing randomness in R")
   (description
    "Several non parametric randomness tests for numeric sequences")
   (license license:gpl2+)))

(define-public r-numbers
  (package
   (name "r-numbers")
   (version "0.7-5")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "numbers" version))
     (sha256
      (base32
       "1dz23rmjzdns4av28kk8bc9psj71q2bpcgm1m7sxnyzcxpm8hczc"))))
   (properties `((upstream-name . "numbers")))
   (build-system r-build-system)
   (home-page
    "https://cran.r-project.org/web/packages/numbers")
   (synopsis "Number-Theoretic Functions")
   (description
    " Provides number-theoretic functions for factorization, prime numbers, twin primes, primitive roots, modular logarithm and inverses, extended GCD, Farey series and continuous fractions.  Includes Legendre and Jacobi symbols, some divisor functions, Euler's Phi function, etc.")
   (license license:gpl3+)))

(define-public r-conf-design
  (package
   (name "r-conf-design")
   (version "2.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "conf.design" version))
     (sha256
      (base32
       "06vdxljkjq1x56xkg041l271an1xv9wq79swxvzzk64dqqnmay51"))))
   (properties `((upstream-name . "conf.design")))
   (build-system r-build-system)
   (home-page
    "https://cran.r-project.org/web/packages/conf.design")
   (synopsis "Construction of factorial designs")
   (description
    "This small library contains a series of simple tools for constructing and manipulating confounded and fractional factorial designs.")
   (license license:gpl2)))

(define-public r-doe-base
  (package
   (name "r-doe-base")
   (version "1.1-5")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "DoE.base" version))
     (sha256
      (base32
       "15m1axp8wpvqdly62chb4p0jzfvmkma6bsw8gagzwm9ibh6xwmkq"))))
   (properties `((upstream-name . "DoE.base")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-combinat" ,r-combinat)
      ("r-conf-design" ,r-conf-design)
      ("r-lattice" ,r-lattice)
      ("r-mass" ,r-mass)
      ("r-numbers" ,r-numbers)
      ("r-partitions" ,r-partitions)
      ("r-vcd" ,r-vcd)))
   (home-page
    "http://prof.beuth-hochschule.de/groemping/DoE/")
   (synopsis
    "Full Factorials, Orthogonal Arrays and Base Utilities for DoE Packages")
   (description
    "Creates full factorial experimental designs and designs based on orthogonal arrays for (industrial) experiments.  Provides diverse quality criteria.  Provides utility functions for the class design, which is also used by other packages for designed experiments.")
   (license license:gpl2+)))

(define-public r-frf2
  (package
   (name "r-frf2")
   (version "2.2-2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "FrF2" version))
     (sha256
      (base32
       "048awbsyzpjmr0kmz3k5yi5vksa9iiffabp6q8xsww097vcngxzb"))))
   (properties `((upstream-name . "FrF2")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-doe-base" ,r-doe-base)
      ("r-igraph" ,r-igraph)
      ("r-scatterplot3d" ,r-scatterplot3d)
      ("r-sfsmisc" ,r-sfsmisc)))
   (home-page
    "http://prof.beuth-hochschule.de/groemping/DoE/")
   (synopsis
    "Fractional Factorial Designs with 2-Level Factors")
   (description
    "Regular and non-regular Fractional Factorial 2-level designs can be created.  Furthermore, analysis tools for Fractional Factorial designs with 2-level factors are offered (main effects and interaction plots for all factors simultaneously, cube plot for looking at the simultaneous effects of three factors, full or half normal plot, alias structure in a more readable format than with the built-in function alias).")
   (license license:gpl2+)))

(define-public r-daewr
  (package
   (name "r-daewr")
   (version "1.2-5")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "daewr" version))
     (sha256
      (base32
       "0cfak3kmw8da7rh04d9qkqypxshj6wanac3318jbc5gvk0injbvd"))))
   (properties `((upstream-name . "daewr")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-frf2" ,r-frf2)
      ("r-lattice" ,r-lattice)
      ("r-stringi" ,r-stringi)))
   (home-page
    "https://cran.r-project.org/web/packages/daewr")
   (synopsis
    "Design and Analysis of Experiments with R")
   (description
    "Contains Data frames and functions used in the book \"Design and Analysis of Experiments with R\".")
   (license license:gpl2)))

(define-public r-qpdf
  (package
   (name "r-qpdf")
   (version "1.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "qpdf" version))
     (sha256
      (base32
       "03lnfncw8qd1fwfyqh1mjvnsjr3b63wxbah0wp5g7z7gba90dwbi"))))
   (properties `((upstream-name . "qpdf")))
   (build-system r-build-system)
   (inputs `(("zlib" ,zlib)
	     ("libjpeg-turbo" ,libjpeg-turbo)))
   (propagated-inputs
    `(("r-askpass" ,r-askpass)
      ("r-curl" ,r-curl)
      ("r-rcpp" ,r-rcpp)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (home-page "https://github.com/ropensci/qpdf")
   (synopsis
    "Split, Combine and Compress PDF Files")
   (description
    "Content-preserving transformations transformations of PDF files such as split, combine, and compress.  This package interfaces directly to the 'qpdf' C++ API and does not require any command line utilities.  Note that 'qpdf' does not read actual content from PDF files: to extract text and data you need the 'pdftools' package.")
   (license license:asl2.0)))

(define-public r-pdftools
  (package
   (name "r-pdftools")
   (version "2.3.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "pdftools" version))
     (sha256
      (base32
       "01i5g2mjkshis0zlm7lrvi7kkzl4dn3if1hzwkgzf9n2mi33ndsx"))))
   (properties `((upstream-name . "pdftools")))
   (build-system r-build-system)
   (inputs `(("zlib" ,zlib)
	     ("poppler" ,poppler)))
   (propagated-inputs
    `(("r-qpdf" ,r-qpdf)
      ("r-rcpp" ,r-rcpp)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (home-page
    "https://docs.ropensci.org/pdftoolshttps://github.com/ropensci/pdftools#readmehttps://poppler.freedesktop.org")
   (synopsis
    "Text Extraction, Rendering and Converting of PDF Documents")
   (description
    "Utilities based on 'libpoppler' for extracting text, fonts, attachments and metadata from a PDF file.  Also supports high quality rendering of PDF documents into PNG, JPEG, TIFF format, or into raw bitmap vectors for further processing in R.")
   (license license:expat)))

(define-public r-tesseract
  (package
   (name "r-tesseract")
   (version "4.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "tesseract" version))
     (sha256
      (base32
       "1a7cf48n7hdd6inqz23ifqhq6kx6wxri34a79ns2vxaj6f4llxf0"))))
   (properties `((upstream-name . "tesseract")))
   (build-system r-build-system)
   (inputs `(("zlib" ,zlib)
	     ("tesseract-ocr" ,tesseract-ocr)
	     ("leptonica" ,leptonica)))
   (propagated-inputs
    `(("r-curl" ,r-curl)
      ("r-digest" ,r-digest)
      ("r-pdftools" ,r-pdftools)
      ("r-rappdirs" ,r-rappdirs)
      ("r-rcpp" ,r-rcpp)))
   (native-inputs
    `(("pkg-config" ,pkg-config)
      ("r-knitr" ,r-knitr)))
   (home-page
    "https://github.com/ropensci/tesseract")
   (synopsis "Open Source OCR Engine")
   (description
    "Bindings to 'Tesseract' <https://opensource.google.com/projects/tesseract>: a powerful optical character recognition (OCR) engine that supports over 100 languages.  The engine is highly configurable in order to tune the detection algorithms and obtain the best possible results.")
   (license license:asl2.0)))

(define-public r-heplots
  (package
   (name "r-heplots")
   (version "1.3-5")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "heplots" version))
     (sha256
      (base32
       "1vyhfkp66gi17jni3gsbv9kn1s0n00qigr13q8xbzbgylz5jjiln"))))
   (properties `((upstream-name . "heplots")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-car" ,r-car) ("r-mass" ,r-mass)))
   (home-page
    "http://datavis.ca/R/index.php#heplots")
   (synopsis
    "Visualizing Hypothesis Tests in Multivariate Linear Models")
   (description
    "This package provides HE plot and other functions for visualizing hypothesis tests in multivariate linear models.  HE plots represent sums-of-squares-and- products matrices for linear hypotheses and for error using ellipses (in two dimensions) and ellipsoids (in three dimensions).  The related 'candisc' package provides visualizations in a reduced-rank canonical discriminant space when there are more than a few response variables.")
   (license license:gpl2+)))

(define-public r-candisc
  (package
   (name "r-candisc")
   (version "0.8-3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "candisc" version))
     (sha256
      (base32
       "0bshi686sad31052glai1wmzxshvafp1vkar7zirds3x737gb0fp"))))
   (properties `((upstream-name . "candisc")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-car" ,r-car) ("r-heplots" ,r-heplots)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "https://cran.r-project.org/web/packages/candisc")
   (synopsis
    "Visualizing Generalized Canonical Discriminant and Canonical Correlation Analysis")
   (description
    "This package provides functions for computing and visualizing generalized canonical discriminant analyses and canonical correlation analysis for a multivariate linear model.  Traditional canonical discriminant analysis is restricted to a one-way 'MANOVA' design and is equivalent to canonical correlation analysis between a set of quantitative response variables and a set of dummy variables coded from the factor variable.  The 'candisc' package generalizes this to higher-way 'MANOVA' designs for all factors in a multivariate linear model, computing canonical scores and vectors for each term.  The graphic functions provide low-rank (1D, 2D, 3D) visualizations of terms in an 'mlm' via the 'plot.candisc' and 'heplot.candisc' methods.  Related plots are now provided for canonical correlation analysis when all predictors are quantitative.")
   (license license:gpl2+)))

(define-public r-smacof
  (package
   (name "r-smacof")
   (version "2.1-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "smacof" version))
     (sha256
      (base32
       "166acgllfwsdb28cjzb49f4b8gwcipqwzi9w45r4qmzyszf89xw3"))))
   (properties `((upstream-name . "smacof")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-candisc" ,r-candisc)
      ("r-colorspace" ,r-colorspace)
      ("r-doparallel" ,r-doparallel)
      ("r-e1071" ,r-e1071)
      ("r-ellipse" ,r-ellipse)
      ("r-foreach" ,r-foreach)
      ("r-hmisc" ,r-hmisc)
      ("r-mass" ,r-mass)
      ("r-nnls" ,r-nnls)
      ("r-plotrix" ,r-plotrix)
      ("r-polynom" ,r-polynom)
      ("r-weights" ,r-weights)
      ("r-wordcloud" ,r-wordcloud)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "https://cran.r-project.org/web/packages/smacof")
   (synopsis "Multidimensional Scaling")
   (description
    "Implements the following approaches for multidimensional scaling (MDS) based on stress minimization using majorization (smacof): ratio/interval/ordinal/spline MDS on symmetric dissimilarity matrices, MDS with external constraints on the configuration, individual differences scaling (idioscal, indscal), MDS with spherical restrictions, and ratio/interval/ordinal/spline unfolding (circular restrictions, row-conditional).  Various tools and extensions like jackknife MDS, bootstrap MDS, permutation tests, MDS biplots, gravity models, unidimensional scaling, drift vectors (asymmetric MDS), classical scaling, and Procrustes are implemented as well.")
   (license license:gpl3)))


(define-public r-pcal1
  (package
   (name "r-pcal1")
   (version "1.5.4")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "pcaL1" version))
     (sha256
      (base32
       "1myl12537bxwmyicm8v891rflq8g6bhz9j0gcjcqgf9z80y52ayc"))))
   (properties `((upstream-name . "pcaL1")))
   (build-system r-build-system)
   (inputs `(("zlib" ,zlib)
	     ("clp" ,clp)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (home-page
    "http://www.optimization-online.org/DB_HTML/2012/04/3436.html")
   (synopsis "L1-Norm PCA Methods")
   (description
    "Implementations of several methods for principal component analysis using the L1 norm.  The package depends on COIN-OR Clp version >= 1.12.0.  The methods implemented are PCA-L1 (Kwak 2008) <DOI:10.1109/TPAMI.2008.114>, L1-PCA (Ke and Kanade 2003, 2005) <DOI:10.1109/CVPR.2005.309>, L1-PCA* (Brooks, Dula, and Boone 2013) <DOI:10.1016/j.csda.2012.11.007>, L1-PCAhp (Visentin, Prestwich and Armagan 2016) <DOI:10.1007/978-3-319-46227-1_37>, wPCA (Park and Klabjan 2016) <DOI: 10.1109/ICDM.2016.0054>, awPCA (Park and Klabjan 2016) <DOI: 10.1109/ICDM.2016.0054>, PCA-Lp (Kwak 2014) <DOI:10.1109/TCYB.2013.2262936>, and SharpEl1-PCA (Brooks and Dula, submitted).")
   (license license:gpl3+)))


(define-public r-transport
  (package
   (name "r-transport")
   (version "0.12-2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "transport" version))
     (sha256
      (base32
       "1d49gm5lwih7b7rav3c42brcp6xi3y55xw11r9n8illjwjayfcxd"))))
   (properties `((upstream-name . "transport")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-data-table" ,r-data-table)
      ("r-rcpp" ,r-rcpp)
      ("r-rcppeigen" ,r-rcppeigen)))
   (home-page "http://www.dominic.schuhmacher.name")
   (synopsis
    "Computation of Optimal Transport Plans and Wasserstein Distances")
   (description
    "Solve optimal transport problems.  Compute Wasserstein distances (a.k.a.  Kantorovitch, Fortet--Mourier, Mallows, Earth Mover's, or minimal L_p distances), return the corresponding transference plans, and display them graphically.  Objects that can be compared include grey-scale images, (weighted) point patterns, and mass vectors.")
   (license license:gpl2+)))

(define-public r-ics
  (package
   (name "r-ics")
   (version "1.3-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ICS" version))
     (sha256
      (base32
       "0x3cwhvzcibgyb8gqy6dc6lgnvbf6x8425zai57g8yn5i6zzc1li"))))
   (properties `((upstream-name . "ICS")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-mvtnorm" ,r-mvtnorm)
      ("r-survey" ,r-survey)))
   (home-page
    "https://cran.r-project.org/web/packages/ICS")
   (synopsis
    "Tools for Exploring Multivariate Data via ICS/ICA")
   (description
    "Implementation of Tyler, Critchley, Duembgen and Oja's (JRSS B, 2009, <doi:10.1111/j.1467-9868.2009.00706.x>) and Oja, Sirkia and Eriksson's (AJS, 2006, <http://www.ajs.or.at/index.php/ajs/article/view/vol35,%20no2%263%20-%207>) method of two different scatter matrices to obtain an invariant coordinate system or independent components, depending on the underlying assumptions.")
   (license license:gpl2+)))

(define-public r-icsnp
  (package
   (name "r-icsnp")
   (version "1.1-1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ICSNP" version))
     (sha256
      (base32
       "1zf0k1kwdmjjqsbiiy3r2l47vjsrg09fj65p6zfld3j4gjbp17fd"))))
   (properties `((upstream-name . "ICSNP")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-ics" ,r-ics)
      ("r-mvtnorm" ,r-mvtnorm)))
   (home-page
    "https://cran.r-project.org/web/packages/ICSNP")
   (synopsis
    "Tools for Multivariate Nonparametrics")
   (description
    "Tools for multivariate nonparametrics, as location tests based on marginal ranks, spatial median and spatial signs computation, Hotelling's T-test, estimates of shape are implemented.")
   (license license:gpl2+)))

(define-public r-fitheavytail
  (package
   (name "r-fitheavytail")
   (version "0.1.2")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "fitHeavyTail" version))
     (sha256
      (base32
       "17jdqxrycqkpmb9pzfqrwcax12j6dx7w122989qrpwqsx48ax133"))))
   (properties `((upstream-name . "fitHeavyTail")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-icsnp" ,r-icsnp)
      ("r-mvtnorm" ,r-mvtnorm)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "https://github.com/dppalomar/fitHeavyTail")
   (synopsis
    "Mean and Covariance Matrix Estimation under Heavy Tails")
   (description
    "Robust estimation methods for the mean vector and covariance matrix from data (possibly containing NAs) under multivariate heavy-tailed distributions such as angular Gaussian (via Tyler's method), Cauchy, and Student's t.  Additionally, a factor model structure can be specified for the covariance matrix.  The package is based on the papers: Sun, Babu, and Palomar (2014), Sun, Babu, and Palomar (2015), Liu and Rubin (1995), and Zhou, Liu, Kumar, and Palomar (2019).")
   (license license:gpl3)))


(define-public r-ecosolver
  (package
   (name "r-ecosolver")
   (version "0.5.4")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ECOSolveR" version))
     (sha256
      (base32 "0973m346vakgddp92dzqlky0wa196hj341r9y7rz67bc2zl8jx2x"))))
   (properties `((upstream-name . "ECOSolveR")))
   (build-system r-build-system)
   (native-inputs (list r-knitr))
   (home-page "https://bnaras.github.io/ECOSolveR/")
   (synopsis "Embedded Conic Solver in R")
   (description
    "R interface to the Embedded COnic Solver (ECOS), an efficient and robust C
library for convex problems.  Conic and equality constraints can be specified in
addition to integer and boolean variable constraints for mixed-integer problems.
 This R interface is inspired by the python interface and has similar calling
conventions.")
   (license license:gpl3+)))

(define-public r-osqp
  (package
  (name "r-osqp")
  (version "0.6.0.5")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "osqp" version))
      (sha256
        (base32 "1djsd2fval9g18vx1dym8bnsnlv8p166zfyzdsj105ydn346kz08"))))
  (properties `((upstream-name . "osqp")))
  (build-system r-build-system)
  (propagated-inputs (list r-matrix r-r6 r-rcpp))
  (home-page "https://osqp.org")
  (synopsis "Quadratic Programming Solver using the 'OSQP' Library")
  (description
    "This package provides bindings to the 'OSQP' solver.  The 'OSQP' solver is a
numerical optimization package or solving convex quadratic programs written in
'C' and based on the alternating direction method of multipliers.  See
<arXiv:1711.08013> for details.")
  (license license:asl2.0)))

(define-public r-scs
  (package
   (name "r-scs")
   (version "3.0-0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "scs" version))
     (sha256
      (base32 "18f1h3j1vzvkr5llqv98026ilrqvrdzckaacscwadzcw99rb2mqz"))))
   (properties `((upstream-name . "scs")))
   (build-system r-build-system)
   (home-page "https://github.com/FlorianSchwendinger/scs")
   (synopsis "Splitting Conic Solver")
   (description
    "Solves convex cone programs via operator splitting.  Can solve: linear programs
('LPs'), second-order cone programs ('SOCPs'), semidefinite programs ('SDPs'),
exponential cone programs ('ECPs'), and power cone programs ('PCPs'), or
problems with any combination of those cones. 'SCS' uses 'AMD' (a set of
routines for permuting sparse matrices prior to factorization) and 'LDL' (a
sparse 'LDL' factorization and solve package) from 'SuiteSparse'
(<https://people.engr.tamu.edu/davis/suitesparse.html>).")
   (license license:gpl3)))

(define-public r-cvxr
  (package
  (name "r-cvxr")
  (version "1.0-10")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "CVXR" version))
      (sha256
        (base32 "0cklfwsr3p4x4w7n6y3h3m4i3si8q0pwyp2vzv7kzawrmqc80484"))))
  (properties `((upstream-name . "CVXR")))
  (build-system r-build-system)
  (propagated-inputs
    (list r-bit64
          r-ecosolver
          r-gmp
          r-matrix
          r-osqp
          r-r6
          r-rcpp
          r-rcppeigen
          r-rmpfr
          r-scs))
  (native-inputs (list r-knitr))
  (home-page "https://cvxr.rbind.io")
  (synopsis "Disciplined Convex Optimization")
  (description
    "An object-oriented modeling language for disciplined convex programming (DCP) as
described in Fu, Narasimhan, and Boyd (2020, <doi:10.18637/jss.v094.i14>).  It
allows the user to formulate convex optimization problems in a natural way
following mathematical convention and DCP rules.  The system analyzes the
problem, verifies its convexity, converts it into a canonical form, and hands it
off to an appropriate solver to obtain the solution.  Interfaces to solvers on
CRAN and elsewhere are provided, both commercial and open source.")
  (license license:asl2.0)))

(define-public r-rceim
  (package
   (name "r-rceim")
   (version "0.3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "RCEIM" version))
     (sha256
      (base32
       "1kil5r88b6lf8vxmswz0wn0hhjxjm8jmlcl5kxjwl6fwjyy2z120"))))
   (properties `((upstream-name . "RCEIM")))
   (build-system r-build-system)
   (home-page
    "https://cran.r-project.org/web/packages/RCEIM")
   (synopsis
    "R Cross Entropy Inspired Method for Optimization")
   (description
    "An implementation of a stochastic heuristic method for performing multidimensional function optimization.  The method is inspired in the Cross-Entropy Method.  It does not relies on derivatives, neither imposes particularly strong requirements into the function to be optimized.  Additionally, it takes profit from multi-core processing to enable optimization of time-consuming functions.")
   (license license:gpl2+)))

(define-public r-oor
  (package
   (name "r-oor")
   (version "0.1.3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "OOR" version))
     (sha256
      (base32
       "13v04rx2q6pbiq7dyd29wvimkr4sbq2f5rnn3a0lcbwr3x35r7h9"))))
   (properties `((upstream-name . "OOR")))
   (build-system r-build-system)
   (home-page "http://github.com/mbinois/OOR")
   (synopsis "Optimistic Optimization in R")
   (description
    "Implementation of optimistic optimization methods for global optimization of deterministic or stochastic functions.  The algorithms feature guarantees of the convergence to a global optimum.  They require minimal assumptions on the (only local) smoothness, where the smoothness parameter does not need to be known.  They are expected to be useful for the most difficult functions when we have no information on smoothness and the gradients are unknown or do not exist.  Due to the weak assumptions, however, they can be mostly effective only in small dimensions, for example, for hyperparameter tuning.")
   (license license:lgpl2.0+)))

(define-public r-sensominer
  (package
   (name "r-sensominer")
   (version "1.26")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "SensoMineR" version))
     (sha256
      (base32
       "0wa6wq8jnk0h5hmx03f8h8pa1g4dw0f3lxrsi93mgz740xd559yf"))))
   (properties `((upstream-name . "SensoMineR")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-algdesign"  ,r-algdesign)
      ("r-cluster"    ,r-cluster)
      ("r-factominer" ,r-factominer)
      ("r-ggplot2"    ,r-ggplot2)
      ("r-ggrepel"    ,r-ggrepel)
      ("r-gtools"     ,r-gtools)
      ("r-kernsmooth" ,r-kernsmooth)
      ("r-reshape2"   ,r-reshape2)))
   (home-page "http://sensominer.free.fr")
   (synopsis "Sensory Data Analysis")
   (description
    "Statistical Methods to Analyse Sensory Data.  SensoMineR: A package for sensory data analysis.  S.  Le and F.  Husson (2008) <DOI:10.1111/j.1745-459X.2007.00137.x>.")
   (license license:gpl2+)))

(define-public r-data-tree
  (package
   (name "r-data-tree")
   (version "1.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "data.tree" version))
     (sha256
      (base32
       "0pizmx2312zsym4m42b97q2184bg3hibvbdrblcga05xln84qrs0"))))
   (properties `((upstream-name . "data.tree")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-r6" ,r-r6)
      ("r-stringi" ,r-stringi)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page "http://github.com/gluc/data.tree")
   (synopsis
    "General Purpose Hierarchical Data Structure")
   (description
    "Create tree structures from hierarchical data, and traverse the tree in various orders.  Aggregate, cumulate, print, plot, convert to and from data.frame and more.  Useful for decision trees, machine learning, finance, conversion from and to JSON, and many other applications.")
   (license license:gpl2+)))

(define-public r-profvis
  (package
   (name "r-profvis")
   (version "0.3.7")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "profvis" version))
     (sha256
      (base32
       "1f86m426pcf90l29hf4hkirzf8f38dihk52bxbdq2gvrrdili5s3"))))
   (properties `((upstream-name . "profvis")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-htmlwidgets" ,r-htmlwidgets)
      ("r-stringr" ,r-stringr)))
   (home-page "https://rstudio.github.io/profvis/")
   (synopsis
    "Interactive Visualizations for Profiling R Code")
   (description
    "Interactive visualizations for profiling R code.")
   (license license:gpl3)))

(define-public r-robinhood
  (package
  (name "r-robinhood")
  (version "1.6.5")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "RobinHood" version))
      (sha256
        (base32 "0j54dhb6bss9dkkphfs18mmnk7n0qmxzykch1ivqqr4m8gbq3x8r"))))
  (properties `((upstream-name . "RobinHood")))
  (build-system r-build-system)
  (propagated-inputs
   (list r-dplyr
	 r-httr
	 r-jsonlite
	 r-lubridate
	 r-magrittr
	 r-profvis
	 r-uuid))
  (home-page "https://github.com/JestonBlu/RobinHood")
  (synopsis "Interface for the RobinHood.com No Commission Investing Platform")
  (description
    "Execute API calls to the RobinHood <https://robinhood.com> investing platform.
Functionality includes accessing account data and current holdings, retrieving
investment statistics and quotes, placing and canceling orders, getting market
trading hours, searching investments by popular tag, and interacting with watch
lists.")
  (license license:gpl3)))

(define-public r-gender
  (package
   (name "r-gender")
   (version "0.6.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gender" version))
     (sha256
      (base32 "18xiaif6iiwjvnjk611sfz7qi5r7mbi73dcrgrkzkjly8zygp42k"))))
   (properties `((upstream-name . "gender")))
   (build-system r-build-system)
   (propagated-inputs (list r-dplyr r-httr r-jsonlite r-remotes))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/lmullen/gender")
   (synopsis "Predict Gender from Names Using Historical Data")
   (description
    "Infers state-recorded gender categories from first names and dates of birth
using historical datasets.  By using these datasets instead of lists of male and
female names, this package is able to more accurately infer the gender of a
name, and it is able to report the probability that a name was male or female.
GUIDELINES: This method must be used cautiously and responsibly.  Please be sure
to see the guidelines and warnings about usage in the 'README' or the package
documentation.  See Blevins and Mullen (2015)
<http://www.digitalhumanities.org/dhq/vol/9/3/000223/000223.html>.")
   (license license:expat)))

(define-public r-predictrace
  (package
   (name "r-predictrace")
   (version "2.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "predictrace" version))
     (sha256
      (base32 "1bk1l9gai4hyck72h81znsm1dzqwm5wddbz0bm9g5igs6zkar2ka"))))
   (properties `((upstream-name . "predictrace")))
   (build-system r-build-system)
   (propagated-inputs (list r-dplyr))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/jacobkap/predictrace")
   (synopsis
    "Predict the Race and Gender of a Given Name Using Census and Social Security Administration Data")
   (description
    "Predicts the most common race of a surname and based on U.S.  Census data, and
the most common first named based on U.S.  Social Security Administration data.")
   (license license:expat)))

(define-public r-duckdb
  (package
  (name "r-duckdb")
  (version "0.3.2-2")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "duckdb" version))
      (sha256
        (base32 "0scsv5gyz71kdrwdjqjrfrrisn1ai3bm4dr514driw5mmjmq8dac"))))
  (properties `((upstream-name . "duckdb")))
  (build-system r-build-system)
  (propagated-inputs (list r-dbi))
  (home-page "https://duckdb.org/")
  (synopsis "DBI Package for the DuckDB Database Management System")
  (description
    "The DuckDB project is an embedded analytical data management system with support
for the Structured Query Language (SQL).  This package includes all of DuckDB
and a R Database Interface (DBI) connector.")
  (license license:expat)))

(define-public r-almanac
  (package
  (name "r-almanac")
  (version "0.1.1")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "almanac" version))
      (sha256
        (base32 "0qsczk74ihy7ac5c4dzd6ax5gfmr56wfnjxg396qc2c3xykb7dd5"))))
  (properties `((upstream-name . "almanac")))
  (build-system r-build-system)
  (propagated-inputs
    (list r-glue r-lubridate r-magrittr r-r6 r-rlang r-v8 r-vctrs))
  (native-inputs (list r-knitr))
  (home-page "https://github.com/DavisVaughan/almanac")
  (synopsis "Tools for Working with Recurrence Rules")
  (description
    "This package provides tools for defining recurrence rules and recurrence
bundles.  Recurrence rules are a programmatic way to define a recurring event,
like the first Monday of December.  Multiple recurrence rules can be combined
into larger recurrence bundles.  Together, these provide a system for adjusting
and generating sequences of dates while simultaneously skipping over dates in a
recurrence bundle's event set.")
  (license license:expat)))

(define-public r-rcppthread
  (package
   (name "r-rcppthread")
   (version "1.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "RcppThread" version))
     (sha256
      (base32
       "1xfcxrny779kgknlvnc4j02ifprnakndnkhx8bhy50d39vp4hjjl"))))
   (properties `((upstream-name . "RcppThread")))
   (build-system r-build-system)
   (home-page
    "https://github.com/tnagler/RcppThread")
   (synopsis "R-Friendly Threading in C++")
   (description
    "This package provides a C++11-style thread class and thread pool that can safely be interrupted from R.  See Nagler (2021) <doi:10.18637/jss.v097.c01>.")
   (license license:expat)))

(define-public r-osmdata
  (package
  (name "r-osmdata")
  (version "0.1.9")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "osmdata" version))
      (sha256
        (base32 "01w6ikqv4cl5x6zs4pzx4fgrdcgia53c9gqwd52f5pmg088pj1x9"))))
  (properties `((upstream-name . "osmdata")))
  (build-system r-build-system)
  (propagated-inputs
    (list r-curl
          r-httr
          r-jsonlite
          r-lubridate
          r-magrittr
          r-rcpp
          r-reproj
          r-rvest
          r-sp
          r-tibble
          r-xml2))
  (native-inputs (list r-knitr))
  (home-page
    "https://docs.ropensci.org/osmdata/https://github.com/ropensci/osmdata/")
  (synopsis
    "Import 'OpenStreetMap' Data as Simple Features or Spatial Objects")
  (description
    "Download and import of 'OpenStreetMap' ('OSM') data as 'sf' or 'sp' objects.
'OSM' data are extracted from the 'Overpass' web server
(<https://overpass-api.de/>) and processed with very fast 'C++' routines for
return to 'R'.")
  (license license:gpl3)))

(define-public r-dodgr
  (package
  (name "r-dodgr")
  (version "0.2.13")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "dodgr" version))
      (sha256
        (base32 "135f76jlj18lm6fpn1k4vjc1vh8v41ww43rrbqjbm1k4gfgqqah5"))))
  (properties `((upstream-name . "dodgr")))
  (build-system r-build-system)
  (propagated-inputs
    (list r-callr
          r-digest
          r-fs
          r-magrittr
          r-osmdata
          r-rcpp
          r-rcppparallel
          r-rcppthread))
  (native-inputs (list r-knitr))
  (home-page "https://github.com/ATFutures/dodgr")
  (synopsis "Distances on Directed Graphs")
  (description
    "Distances on dual-weighted directed graphs using priority-queue shortest paths
(Padgham (2019) <doi:10.32866/6945>).  Weighted directed graphs have weights
from A to B which may differ from those from B to A.  Dual-weighted directed
graphs have two sets of such weights.  A canonical example is a street network
to be used for routing in which routes are calculated by weighting distances
according to the type of way and mode of transport, yet lengths of routes must
be calculated from direct distances.")
  (license license:gpl3)))

(define-public r-spacetime
  (package
   (name "r-spacetime")
   (version "1.2-4")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "spacetime" version))
     (sha256
      (base32
       "0khjq2x0bgmf0l6nrphx4fz9c03b87g5v48qb5myagvagscwq70z"))))
   (properties `((upstream-name . "spacetime")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-intervals" ,r-intervals)
      ("r-lattice" ,r-lattice)
      ("r-sp" ,r-sp)
      ("r-xts" ,r-xts)
      ("r-zoo" ,r-zoo)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page "https://github.com/edzer/spacetime")
   (synopsis
    "Classes and Methods for Spatio-Temporal Data")
   (description
    "Classes and methods for spatio-temporal data, including space-time regular lattices, sparse lattices, irregular data, and trajectories; utility functions for plotting data as map sequences (lattice or animation) or multiple time series; methods for spatial and temporal selection and subsetting, as well as for spatial/temporal/spatio-temporal matching or aggregation, retrieving coordinates, print, summary, etc.")
   (license license:gpl2+)))

(define-public r-gstat
  (package
   (name "r-gstat")
   (version "2.0-7")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gstat" version))
     (sha256
      (base32
       "17d95f4s49a8v197ij8ppvl55fc63v1z2qsisniw8hl17gj7q6z8"))))
   (properties `((upstream-name . "gstat")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-fnn" ,r-fnn)
      ("r-lattice" ,r-lattice)
      ("r-sp" ,r-sp)
      ("r-spacetime" ,r-spacetime)
      ("r-zoo" ,r-zoo)))
   (home-page "https://github.com/r-spatial/gstat/")
   (synopsis
    "Spatial and Spatio-Temporal Geostatistical Modelling, Prediction and Simulation")
   (description
    "Variogram modelling; simple, ordinary and universal point or block (co)kriging; spatio-temporal kriging; sequential Gaussian or indicator (co)simulation; variogram and variogram map plotting utility functions; supports sf and stars.")
   (license license:gpl2+)))

(define-public r-shinywidgets
  (package
   (name "r-shinywidgets")
   (version "0.6.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "shinyWidgets" version))
     (sha256
      (base32
       "140g5vz1hsfqldy1f7nwnnqk759m07z92fmlhl5w2va6niv4hjkn"))))
   (properties `((upstream-name . "shinyWidgets")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-bslib" ,r-bslib)
      ("r-htmltools" ,r-htmltools)
      ("r-jsonlite" ,r-jsonlite)
      ("r-sass" ,r-sass)
      ("r-shiny" ,r-shiny)))
   (home-page
    "https://github.com/dreamRs/shinyWidgets")
   (synopsis "Custom Inputs Widgets for Shiny")
   (description
    "Collection of custom input controls and user interface components for 'Shiny' applications.  Give your applications a unique and colorful style !")
   (license license:gpl3)))

(define-public r-shinyfeedback
  (package
   (name "r-shinyfeedback")
   (version "0.3.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "shinyFeedback" version))
     (sha256
      (base32
       "0cqp3a2nngbvrr04ajavnmpbr366zb11iwni4fs8v6ymx8v1wmd7"))))
   (properties `((upstream-name . "shinyFeedback")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-htmltools" ,r-htmltools)
      ("r-jsonlite" ,r-jsonlite)
      ("r-shiny" ,r-shiny)))
   (native-inputs `(("r-knitr" ,r-knitr)))
   (home-page
    "https://github.com/merlinoa/shinyFeedback")
   (synopsis "Display User Feedback in Shiny Apps")
   (description
    "Easily display user feedback in Shiny apps.")
   (license license:expat)))

(define-public r-clpapi
  (package
   (name "r-clpapi")
   (version "1.3.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "clpAPI" version))
     (sha256
      (base32
       "1ivrvipnvnqf1mnvvqnv2jgf69i0h84f46n8x41bx89lag0161bv"))))
   (properties `((upstream-name . "clpAPI")))
   (build-system r-build-system)
   (inputs `(("zlib" ,zlib)
	     ("clp" ,clp)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (home-page
    "https://cran.r-project.org/package=clpAPI")
   (synopsis "R Interface to C API of COIN-or Clp")
   (description
    "R Interface to C API of COIN-OR Clp, depends on COIN-OR Clp Version >= 1.12.0.")
   (license license:gpl3)))

(define-public r-targets
  (package
   (name "r-targets")
   (version "0.12.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "targets" version))
     (sha256
      (base32 "096hw48253izfc2chyk5c4dplw5ha669nx7ca447hs8p0cniijjz"))))
   (properties `((upstream-name . "targets")))
   (build-system r-build-system)
   (propagated-inputs
    (list r-base64url
          r-callr
          r-cli
          r-codetools
          r-data-table
          r-digest
          r-igraph
          r-knitr
          r-r6
          r-rlang
          r-tibble
          r-tidyselect
          r-vctrs
          r-withr
          r-yaml))
   (native-inputs (list r-knitr))
   (home-page "https://docs.ropensci.org/targets/")
   (synopsis "Dynamic Function-Oriented 'Make'-Like Declarative Workflows")
   (description
    "As a pipeline toolkit for Statistics and data science in R, the 'targets'
package brings together function-oriented programming and 'Make'-like
declarative workflows.  It analyzes the dependency relationships among the tasks
of a workflow, skips steps that are already up to date, runs the necessary
computation with optional parallel workers, abstracts files as R objects, and
provides tangible evidence that the results match the underlying code and data.
The methodology in this package borrows from GNU 'Make' (2015,
ISBN:978-9881443519) and 'drake' (2018, <doi:10.21105/joss.00550>).")
   (license license:expat)))

(define-public r-tarchetypes
  (package
   (name "r-tarchetypes")
   (version "0.6.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "tarchetypes" version))
     (sha256
      (base32 "1knzzbfxka8n1vvz7ikplckqzrdw62fqy5wkh46fvgcb13dmx99a"))))
   (properties `((upstream-name . "tarchetypes")))
   (build-system r-build-system)
   (propagated-inputs
    (list r-digest
          r-dplyr
          r-fs
          r-rlang
          r-targets
          r-tibble
          r-tidyselect
          r-vctrs
          r-withr))
   (home-page "https://docs.ropensci.org/tarchetypes/")
   (synopsis "Archetypes for Targets")
   (description
    "Function-oriented Make-like declarative workflows for Statistics and data
science are supported in the 'targets' R package.  As an extension to 'targets',
the 'tarchetypes' package provides convenient user-side functions to make
'targets' easier to use.  By establishing reusable archetypes for common kinds
of targets and pipelines, these functions help express complicated reproducible
workflows concisely and compactly.  The methods in this package were influenced
by the 'drake' R package by Will Landau (2018) <doi:10.21105/joss.00550>.")
   (license license:expat)))

(define-public r-narray
  (package
   (name "r-narray")
   (version "0.4.1.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "narray" version))
     (sha256
      (base32 "1xyyn6v3khk5x759fr9h96z036hbb87fzfqgdpw907bc95gjnkz9"))))
   (properties `((upstream-name . "narray")))
   (build-system r-build-system)
   (propagated-inputs (list r-progress r-stringr))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/mschubert/narray")
   (synopsis "Subset- And Name-Aware Array Utility Functions")
   (description
    "Stacking arrays according to dimension names, subset-aware splitting and mapping
of functions, intersecting along arbitrary dimensions, converting to and from
data.frames, and many other helper functions.")
   (license license:expat)))

(define-public r-clustermq
  (package
   (name "r-clustermq")
   (version "0.8.95.3")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "clustermq" version))
     (sha256
      (base32 "1h6s31ni8q5x062sgp7mhm1aw099nsj21diw00mr3m76yizsram7"))))
   (properties `((upstream-name . "clustermq")))
   (build-system r-build-system)
   (inputs (list zeromq zlib))
   (propagated-inputs (list r-narray r-progress r-purrr r-r6 r-rcpp))
   (native-inputs (list pkg-config r-knitr))
   (home-page "https://mschubert.github.io/clustermq/")
   (synopsis
    "Evaluate Function Calls on HPC Schedulers (LSF, SGE, SLURM, PBS/Torque)")
   (description
    "Evaluate arbitrary function calls using workers on HPC schedulers in single line
of code.  All processing is done on the network without accessing the file
system.  Remote schedulers are supported via SSH.")
   (license license:expat)))

(define-public r-qrencoder
  (package
    (name "r-qrencoder")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "qrencoder" version))
       (sha256
        (base32 "1lg60lg2fiqdw0m228i8pln2p0kqp9f21qmrx6r6rwxifvwlfhv8"))))
    (properties `((upstream-name . "qrencoder")))
    (build-system r-build-system)
    (propagated-inputs (list r-base64enc r-png r-raster r-rcpp))
    (home-page "http://github.com/hrbrmstr/qrencoder")
    (synopsis "Quick Response Code (QR Code) / Matrix Barcode Creator")
    (description
     "Quick Response codes (QR codes) are a type of matrix bar code and can be used to
authenticate transactions, provide access to multi-factor authentication
services and enable general data transfer in an image.  QR codes use four
standardized encoding modes (numeric, alphanumeric, byte/binary, and kanji) to
efficiently store data.  Matrix barcode generation is performed efficiently in C
via the included 'libqrencoder' library created by Kentaro Fukuchi.")
    (license license:gpl2)))

(define-public r-qrcode
  (package
   (name "r-qrcode")
   (version "0.1.4")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "qrcode" version))
     (sha256
      (base32 "1ajn7lmyaxyk1dgmhz5y8cl61g23g0cjva68ac604ffa14kfv76q"))))
   (properties `((upstream-name . "qrcode")))
   (build-system r-build-system)
   (propagated-inputs (list r-assertthat r-r-utils r-stringr))
   (home-page "https://thierryo.github.io/qrcode/")
   (synopsis "Generate QRcodes with R")
   (description "Create QRcode in R.")
   (license license:gpl3)))

(define-public r-av
  (package
   (name "r-av")
   (version "0.7.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "av" version))
     (sha256
      (base32 "004crdrsgh2dak64zmr5w0pj94vpalgmb76g1cb452djavbp7rxl"))))
   (properties `((upstream-name . "av")))
   (build-system r-build-system)
   (inputs (list zlib ffmpeg))
   (native-inputs (list pkg-config))
   (home-page "https://docs.ropensci.org/av/")
   (synopsis "Working with Audio and Video in R")
   (description
    "Bindings to 'FFmpeg' <http://www.ffmpeg.org/> AV library for working with audio
and video in R.  Generates high quality video from images or R graphics with
custom audio.  Also offers high performance tools for reading raw audio,
creating 'spectrograms', and converting between countless audio / video formats.
 This package interfaces directly to the C API and does not require any command
line utilities.")
   (license license:expat)))

(define-public r-gpg
  (package
   (name "r-gpg")
   (version "1.2.8")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "gpg" version))
            (sha256
             (base32
              "1yysjv335mq6lpdc01yiv56zhs0085bdlanh74k3ahjbszh9cy27"))))
   (properties `((upstream-name . "gpg")))
   (build-system r-build-system)
   (propagated-inputs (list r-askpass r-curl))
   (native-inputs (list pkg-config r-knitr gpgme gnupg))
   (home-page "https://github.com/jeroen/gpg")
   (synopsis "GNU Privacy Guard for R")
   (description
    "Bindings to GnuPG for working with OpenGPG (RFC4880) cryptographic methods.
Includes utilities for public key encryption, creating and verifying digital
signatures, and managing your local keyring.  Some functionality depends on the
version of GnuPG that is installed on the system.  On Windows this package can
be used together with GPG4Win which provides a GUI for managing keys and
entering passphrases.")
   (license license:expat)))

(define-public r-kfas
  (package
   (name "r-kfas")
   (version "1.4.6")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "KFAS" version))
            (sha256
             (base32
              "1395xqm8rbg0i2vrd9n83fslf8v40csh0h17wczdnwnff3z3gpfy"))))
   (properties `((upstream-name . "KFAS")))
   (build-system r-build-system)
   (native-inputs (list gfortran r-knitr))
   (home-page "https://github.com/helske/KFAS")
   (synopsis
    "Kalman Filter and Smoother for Exponential Family State Space Models")
   (description
    "State space modelling is an efficient and flexible framework for statistical
inference of a broad class of time series and other data.  KFAS includes
computationally efficient functions for Kalman filtering, smoothing,
forecasting, and simulation of multivariate exponential family state space
models, with observations from Gaussian, Poisson, binomial, negative binomial,
and gamma distributions.  See the paper by Helske (2017)
<doi:10.18637/jss.v078.i10> for details.")
   (license license:gpl2+)))

(define-public r-marss
  (package
   (name "r-marss")
   (version "3.11.4")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "MARSS" version))
            (sha256
             (base32
              "10mf1vs51yqqg6s2435j84xbaxvr6wdraha07mqlr2rmprgl5a0r"))))
   (properties `((upstream-name . "MARSS")))
   (build-system r-build-system)
   (propagated-inputs (list r-kfas r-mvtnorm r-nlme))
   (home-page "https://atsa-es.github.io/MARSS/")
   (synopsis "Multivariate Autoregressive State-Space Modeling")
   (description
    "The MARSS package provides maximum-likelihood parameter estimation for
constrained and unconstrained linear multivariate autoregressive state-space
(MARSS) models fit to multivariate time-series data.  Fitting is primarily via
an Expectation-Maximization (EM) algorithm, although fitting via the BFGS
algorithm (using the optim function) is also provided.  MARSS models are a class
of dynamic linear model (DLM) and vector autoregressive model (VAR) model.
Functions are provided for parametric and innovations bootstrapping, Kalman
filtering and smoothing, bootstrap model selection criteria (AICb), confidences
intervals via the Hessian approximation and via bootstrapping and calculation of
auxiliary residuals for detecting outliers and shocks.  The user guide shows
examples of using MARSS for parameter estimation for a variety of applications,
model selection, dynamic factor analysis, outlier and shock detection, and
addition of covariates.  Online workshops (lectures, eBook, and computer labs)
at <https://atsa-es.github.io/>  See the NEWS file for update information.")
   (license license:gpl2)))

(define-public r-softbart
  (package
   (name "r-softbart")
   (version "1.0.1")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "SoftBart" version))
            (sha256
             (base32
              "0s7d2gxh6qnpwzagfrwhacgd7w3hxx3xkxrhk6kqjykpckdwy091"))))
   (properties `((upstream-name . "SoftBart")))
   (build-system r-build-system)
   (propagated-inputs (list r-caret
                            r-glmnet
                            r-mass
                            r-progress
                            r-rcpp
                            r-rcpparmadillo
                            r-scales
                            r-truncnorm))
   (home-page "https://cran.r-project.org/package=SoftBart")
   (synopsis "Implements the SoftBart Algorithm")
   (description
    "This package implements the @code{SoftBart} model of described by Linero and
Yang (2018) <doi:10.1111/rssb.12293>, with the optional use of a
sparsity-inducing prior to allow for variable selection.  For usability, the
package maintains the same style as the @code{BayesTree} package.")
   (license license:gpl2+)))

(define-public r-tsss
  (package
   (name "r-tsss")
   (version "1.3.4-5")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "TSSS" version))
     (sha256
      (base32 "13d58fkjll08hznpawl1ln93vz5sy65k8j92kxz4kkpw2jdg690h"))))
   (properties `((upstream-name . "TSSS")))
   (build-system r-build-system)
   (native-inputs (list gfortran))
   (home-page "https://cran.r-project.org/package=TSSS")
   (synopsis "Time Series Analysis with State Space Model")
   (description
    "This package provides functions for statistical analysis, modeling and
simulation of time series with state space model, based on the methodology in
Kitagawa (2020, ISBN: 978-0-367-18733-0).")
   (license license:gpl2+)))
