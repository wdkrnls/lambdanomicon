(define-module (lambdanomicon packages r-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system r)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (gnu packages cran)
  #:use-module (lambdanomicon packages cran)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages gcc))

(define-public r-gnufind
  (package
  (name "r-gnufind")
  (version "0.1.2")
  (source
   (origin
     (method git-fetch)
     (uri (git-reference
	   (url "http://github.com/wdkrnls/gnufind")
	   (commit "master")))
     (sha256
      (base32
       "1lcvm915y2qs14zrs07qrsc6i7wvs3ks1dwbq10sn9ymv2n1xfqb"))))
  (build-system r-build-system)
  (home-page "http://github.com/wdkrnls/gnufind")
  (synopsis "R wrapper around GNU find shell search utility.")
  (description
   "R wrapper around GNU find shell search utility. Also provides
helper functions for forming compound expressions.")
  (license gpl3+)))

(define-public r-warpcore
  (package
   (name "r-warpcore")
   (version "0.5.22")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "http://gitlab.com/wdkrnls/warpcore")
	   (commit "master")))
     (sha256
      (base32
       "0ydvvygcv76cdmvfnhdrip99973fisw8s7bvjy5hv654nzm8j1y7"))))
   (build-system r-build-system)
   (propagated-inputs `(("r-rgenoud"  ,r-rgenoud)))
   (synopsis "Kyle's Gaussian process playground.")
   (home-page "http://gitlab.com/wdkrnls/warpcore")
   (description "My personal package for studying Gaussian processes.")
   (license gpl3)))


(define-public r-rgraphm
  (package
   (name "r-rgraphm")
   (version "0.1.12")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/adalisan/rgraphm")
	   (commit "master")))
     (sha256
      (base32
       "1ng2z890ci4303ll28r6scfxq5praqn64pli1f7ddlqic7jssrg9"))))
   (build-system r-build-system)
   (propagated-inputs `(("r-rcppgsl" ,r-rcppgsl)
			("r-rcpp" ,r-rcpp)))
   (synopsis "Approximate graph matching algorithms")
   (home-page "https://github.com/adalisan/RGraphM")
   (description "Graph matching algorithms developed between 2000-2009.")
   (license gpl3)))
(define-public r-alpacaforr
  (package
   (name "r-alpacaforr")
   (version "1.0.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/yogat3ch/AlpacaforR.git")
	   (commit "master")))
     (sha256
      (base32
       "0i1ncyf4w9kcgxsqsnf19bx5cyafvnv6chgfp6fr69qdxx5wpyqc"))))
   (build-system r-build-system)
   (propagated-inputs `(("r-dplyr" ,r-dplyr)
			("r-glue" ,r-glue)
			("r-httr" ,r-httr)
			("r-jsonlite" ,r-jsonlite)
			("r-lubridate" ,r-lubridate)
			("r-purrr" ,r-purrr)
			("r-rlang" ,r-rlang)
			("r-stringr" ,r-stringr)
			("r-tibble" ,r-tibble)
			("r-websocket" ,r-websocket)))
   (synopsis "Trade with Alpaca using R")
   (home-page "https://github.com/yogat3ch/AlpacaforR")
   (description "Interact with the Alpaca API and the subset of
endpoints on the Polygon API` available to Alpaca members. All Alpaca
endpoints are accessible with AlpacaforR: Account, Assets, Calendar,
Clock, Market-Data, Orders, Polygon, Positions, Watchlist and
Streaming. Get asset, account and order info, market calendar info and
historical market data. Submit, cancel and replace simple and complex
orders, see market open / closing information, and much more. Build
and test strategies with a paper account, then implement them live
with an Alpaca Brokerage account. If you'd like to provide feedback,
submit an issue, express thanks, or donate to support the project,
please do so on Github: github.com/jagg19/AlpacaforR")
   (license gpl3)))
