;;; Lambdanomicon
;;; Copyright © 2020 Kyle Andrews <kyle.c.andrews@gmail.com>
;;;
;;; This file is part of Lambdanomicon
;;;
;;; Lambdanomicon is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.

(define-module (lambdanomicon packages python-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gtk)
  #:use-module ((guix licenses) #:prefix license:))

(define-public python-daff
  (package
    (name "python-daff")
    (version "1.3.43")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "daff" version))
       (sha256
        (base32
	 "0l1hrijbqjqvwqfy5pxx2pn7bq6zgrlfbmyb2b47z9zd3bizcm4f"))))
    (build-system python-build-system)
    (home-page "https://github.com/paulfitz/daff")
    (synopsis "Diff and patch tables")
    (description "Diff and patch CSV/JSON/SQLite tables. Can be used with git.")
    (license license:expat)))

(define-public python-ase
  (package
    (name "python-ase")
    (version "3.19.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ase" version))
       (sha256
        (base32
	 "03xzpmpask2q2609kkq0hfgzsfvkyjpbjws7qx00nnfrbbnjk443"))))
    (build-system python-build-system)
    (propagated-inputs
     `(("python-matplotlib" ,python-matplotlib)
       ("python-numpy" ,python-numpy)
       ("python-scipy" ,python-scipy)))
    (home-page "https://wiki.fysik.dtu.dk/ase")
    (synopsis "Atomic Simulation Environment")
    (description "Atomic Simulation Environment")
    (license license:lgpl3+)))

(define-public python-zim
  (package
    (name "python-zim")
    (version "0.73.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://zim-wiki.org/"
                                  "downloads/zim-" version ".tar.gz"))
       (sha256
        (base32 "13vhwsgv6mscgixypc0ixkgj0y7cpcm7z7wn1vmdrwp7kn8m3xgx"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:use-setuptools? #f))
    (propagated-inputs
     `(("gtk+" ,gtk+)
       ("python" ,python)
       ("python-pygobject" ,python-pygobject)
       ("python-pyxdg" ,python-pyxdg)
       ("xdg-utils" ,xdg-utils)))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (home-page "https://zim-wiki.org/")
    (synopsis "Zim Desktop Wiki")
    (description
     "Zim is a graphical text editor used to maintain a collection of wiki pages. Each page can contain links to other pages, simple formatting and images. Pages are stored in a folder structure, like in an outliner, and can have attachments. Creating a new page is as easy as linking to a nonexistent page. All data is stored in plain text files with wiki formatting. Various plugins provide additional functionality, like a task list manager, an equation editor, a tray icon, and support for version control.")
    (license license:gpl2)))

(define-public python-apted
  (package
    (name "python-apted")
    (version "1.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "apted" version))
       (sha256
        (base32
         "1sawf6s5c64fgnliwy5w5yxliq2fc215m6alisl7yiflwa0m3ymy"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/JoaoFelipe/apted")
    (synopsis
     "APTED algorithm for the Tree Edit Distance")
    (description
     "APTED algorithm for the Tree Edit Distance")
    (license license:expat)))

(define-public repofs
  (package
   (name "repofs")
   (version "v0.2.6")
   (source
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/AUEB-BALab/RepoFS")
            (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32
        "1cy4sn42d6q02kx29hrdlimq17phpk0rzp94n5wvlmvw6393xpwh"))))
   (build-system python-build-system)
   (inputs
    (list python-pygit2 python-fusepy))
   (home-page "https://github.com/AUEB-BALab/RepoFS")
   (synopsis
    "Virtual File Server for exploring a Git repository.")
   (description
    "RepoFS represents the Git version control system as a virtual file system where
 all commits and their contents can be traversed concurrently in a read-only manner
 using shell commands.")
   (license license:asl2.0)))
