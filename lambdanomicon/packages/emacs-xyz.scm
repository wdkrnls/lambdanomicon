(define-module (lambdanomicon packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system emacs)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages texinfo)
  #:use-module (guix git-download)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-keycast
  (package
    (name "emacs-keycast")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/tarsius/keycast.git")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name (string-append "v" version)))
              (sha256
               (base32
		"1saa4ngw1z16vk33fvn4lc4ld3dplrgbbmdqy1lrfxjy3fyw62q4"))))
    (build-system emacs-build-system)
    (home-page
     "https://github.com/tarsius/keycast")
    (synopsis "Show current command and its key in the mode line.")
    (description
     "KeyCast helps make educational screencasts which show exactly
what keys are being pressed in the Emacs mode-line.

See also emacs-gif-screencast.")
    (license license:gpl3+)))

(define-public emacs-leuven-theme
  (package
    (name "emacs-leuven-theme")
    (version "026da5d614864a60bb151f0e75240a938e41923b")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/fniessen/emacs-leuven-theme.git")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
		"008ynbzcszsbyj3pi9cm6ig1ks059xprkyhd74dnw8grlddwfimd"))))
    (build-system emacs-build-system)
    (home-page
     "https://github.com/fniessen/emacs-leuven-theme")
    (synopsis "Nice theme for Emacs.")
    (description "The light version of this theme is built in to Emacs
proper. This version provides a dark variant.")
    (license license:gpl3+)))

(define-public emacs-polymode-markdown
  (package
   (name "emacs-polymode-markdown")
   (version "0.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/polymode/poly-markdown.git")
	   (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0b6wlmhrpcw9g8rbw7q7k5fr2lgcp1rpy7d9p9f0gzn52yvcr4dr"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("emacs-polymode" ,emacs-polymode)
      ("emacs-markdown-mode" ,emacs-markdown-mode)))
    (properties '((upstream-name . "poly-markdown")))
    (home-page "https://github.com/polymode/poly-markdown")
    (synopsis "Polymode definitions for Markdown buffers")
    (description
     "Provides definitions for @code{emacs-polymode} to support
@code{markdown} buffers.  Edit source blocks in an markdown-mode buffer using the
native modes of the blocks' languages while remaining inside the primary markdown
buffer.")
    (license license:gpl3+)))


(define-public emacs-polymode-noweb
  (package
   (name "emacs-polymode-noweb")
   (version "0.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/polymode/poly-noweb.git")
	   (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1pnjg615i5p9h5fppvn36vq2naz4r1mziwqjwwxka6kic5ng81h8"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("emacs-polymode" ,emacs-polymode)))
    (properties '((upstream-name . "poly-noweb")))
    (home-page "https://github.com/polymode/poly-noweb")
    (synopsis "Polymode definitions for noweb buffers")
    (description
     "Provides definitions for @code{emacs-polymode} to support
@code{noweb} buffers.  Edit source blocks in an noweb buffer using the
native modes of the blocks' languages while remaining inside the
primary buffer.")
    (license license:gpl3+)))

(define-public emacs-polymode-r
  (package
    (name "emacs-polymode-r")
    (version "0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/polymode/poly-r.git")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1v5djxwgqksf84pxfpgbm7qaz3yq5ha7cac0792p62pj1ydzvghi"))))
    (build-system emacs-build-system)
    (propagated-inputs
     `(("emacs-polymode" ,emacs-polymode)
       ("emacs-polymode-noweb" ,emacs-polymode-noweb)
       ("emacs-polymode-markdown" ,emacs-polymode-markdown)
       ("emacs-ess" ,emacs-ess)))
    (properties '((upstream-name . "poly-R")))
    (home-page "https://github.com/polymode/poly-R")
    (synopsis "Polymode definitions for R markdown documents.")
    (description
     "Provides definitions for @code{emacs-polymode} to support
@code{.Rmd} files.  Edit source blocks in a R mode buffer using the
native modes of the blocks' languages while remaining inside the primary Markdown
buffer.")
    (license license:gpl3+)))

(define-public emacs-drag-stuff
  (package
    (name "emacs-drag-stuff")
    (version "0.3.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rejeep/drag-stuff.el.git")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1jrr59iazih3imkl9ja1lbni9v3xv6b8gmqs015g2mxhlql35jka"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/rajeep/drag-stuff.el")
    (synopsis "Drag stuff around in text buffers within GNU Emacs.")
    (description
     "Drag Stuff is a minor mode for Emacs that makes it possible to 
      drag stuff (words, region, lines) around in Emacs.")
    (license license:gpl3+)))
