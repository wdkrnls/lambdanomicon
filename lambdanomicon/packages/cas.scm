;;; Lambdanomicon
;;; Copyright © 2019 Kyle Andrews <kyle.c.andrews@gmail.com>
;;;
;;; This file is part of Lambdanomicon
;;;
;;; Lambdanomicon is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.

(define-module (lambdanomicon packages cas)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages pkg-config))

(define-public fricas
  (package
    (name "fricas")
    (version "1.3.5")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
	     "https://sourceforge.net/projects/fricas/files/fricas/"
	     version "/fricas-" version "-full.tar.bz2"))
       (sha256
	(base32
	 "0mpal0dwyd3g2v03vf3panx302cx6n30xy4cxmkdypzvm6wkkrh8"))))
    (build-system gnu-build-system)
  (propagated-inputs
   `(("sbcl"        ,sbcl)
     ("libx11"      ,libx11)
     ("libxpm"      ,libxpm)
     ("libxt"       ,libxt)
     ("libsm"       ,libsm)
     ("libice"      ,libice)
     ("libxau"      ,libxau)
     ("libxdmcp"    ,libxdmcp)))
  (native-inputs
   `(("pkg-config"  ,pkg-config)))
    (synopsis "Computer algebra system (CAS) based on abstract algebra
and category theory.")
    (description "FriCAS is an advanced computer algebra system. Its
capabilities range from calculus (integration and differentiation) to
abstract algebra. It can plot functions and has integrated help
system.")
    (home-page "http://fricas.sourceforge.net/")
    (license license:bsd-3)))
