(define-module (lambdanomicon packages misc-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pkg-config))

(define-public lssecret
    (package
    (name "lssecret")
    (version "1")
    (source
     (origin
      (method git-fetch)
      (uri
       (git-reference
	(url "https://github.com/jw0k/lssecret.git")
	(commit "4a7e00a")))
      (sha256
       (base32
	"0sacshnkibs4h4nqrr6fdl2gvvdrrkwpyjr515qpkrwx32n34jfm"))))
    (properties `((upstream-name . "lssecret")))
    (build-system gnu-build-system)
    (native-inputs
     (list pkg-config libsecret))
    (arguments
     (list #:make-flags #~(list (string-append "DESTDIR=" #$output))
	   #:phases #~(modify-phases %standard-phases
			(delete 'configure)
			(delete 'check))))
    (home-page
     "https://github.com/jw0k/lssecret")
    (synopsis
     "lssecret provides a command line tools for seeing the secrets managed by libsecret.")
    (description
     "lssecret interfaces with libsecret but without all that confusing GNOME mumbo jumbo.")
    (license license:public-domain)))
